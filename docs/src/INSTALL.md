{{#include links.md}}

## Dependencies

Keyfork has different dependencies depending on the feature set used for
installation, but the default build dependencies may be installed on a Debian
system by running:

```sh
sudo apt install pkg-config nettle-dev libpcsclite-dev clang llvm
```

The runtime dependencies are:

```sh
sudo apt install libnettle8 libpcsclite1 pcscd
```

## Installing Keyfork

Keyfork is hosted using the Distrust Cargo repository. For the fastest
installation path (this is not recommended), crates may be installed directly
from the Cargo repository:

```sh
cargo install --index https://git.distrust.co/public/_cargo-index keyfork@0.1.0
```

The index is managed by Distrust, but is not signed by developers when commits
are created, so a safer alternative may be to build from source. It is
recommended to perform these operations in an environment dedicated for the
purpose of building Rust binaries, to avoid the risk of building a compromised
binary.

```sh
git clone https://git.distrust.co/public/keyfork
cd keyfork
# git checkout keyfork-0.1.0
git verify-commit HEAD
cargo install --locked --path crates/keyfork
```

This will build Keyfork from source, using a local `Cargo.lock` file to ensure
dependencies are not updated automatically.

## Installing Plumbing Binaries

Keyfork offers "plumbing" binaries (as opposed to the "porcelain" `keyfork`)
that offer a smaller [SBOM], allowing users with a smaller feature requirement
to lessen the requirements for code review. Plumbing binaries can be installed
the same way Keyfork is installed, either through the registry or by building
locally. Plumbing binaries may be grouped by crates of shared dependencies if
all dependencies are shared between the binaries. Plumbing binaries may also
exist in crates that exist only to serve as a command-line interface to that
crate, but may require additional dependencies to build a binary. These will be
specified by `--features bin` as a flag that should be passed to `cargo
install`.

<!-- TODO:
  Should plumbing binaries be their own packages?
  A convenient command to find the package for a binary should be provided.
  -->

```sh
# Currently (2024-01-17) unreleased, may not exist yet
cargo install --index https://git.distrust.co/public/_cargo-index keyfork-entropy@0.1.0 --features bin

# Confirmed to work as of 2024-01-17.
cargo install --locked --path crates/util/keyfork-entropy --bin keyfork-entropy --features bin
```
