{{#include links.md}}

# Keyfork Shard Commands

Sharding a seed allows "M-of-N" recovery of the seed, which is useful for
disaster recovery situations, avoiding the case where a single person may be
left in charge of the seed. Keyfork provides utilities to help with the
management of seeds through sharding.

## Creating a Seed and Shard File

If using smartcards dedicated to this use case, Keyfork offers a wizard to
generate a seed and encrypt it to multiple factory-reset smartcards. Assuming
you want to use 2-of-4 shards to recreate the secret, run the following
command:

```sh
keyfork wizard generate-shard-secret --threshold 2 --max 4 > shards.pgp
```

Keyfork will prompt for keys to be inserted and for PINs to be configured for
the smartcards. The shards file should be copied to every system intended to
decrypt shards.

## Starting Keyfork using a single system

If all shardholders are present, the following command can be run to start the
Keyfork server process:

```sh
keyfork recover shard shards.pgp
```

Keyfork will prompt for keys to be inserted and for the User PINs for the keys
to be entered. Once the shard is decrypted, the Keyfork server will start.

## Starting Keyfork using remote systems

A line of communication should be established with the shardholders, but can be
public and/or recorded. On the system intended to run the Keyfork server, the
following command can be run:

```sh
keyfork recover remote-shard
```

The command will continuously prompt a QR code, followed by 33 words, to be
sent to the remote operator. The operator must then perform their operations
and send back their own QR code, optionally followed by 72 words. The QR code
must be scanned by Keyfork, else the 72 words will be required.

### Shard Transport

Upon receiving the QR code and/or the 33 words, Shardholders should run the
following command to transport their shards:

```sh
keyfork shard transport < shards.pgp
```

The QR code must be scanned by Keyfork, else the 33 words will be required.
Once entered, Keyfork will prompt with a new QR code and 72 words. A picture of
the QR code and (if requested by the lead operator) 72 words should be sent
back.

## Example: Deriving an OpenPGP key for Encryption

Once the Keyfork server has started, Keyfork can be used to derive an OpenPGP
key, and [Sequoia] can be used to extract the public portions:

```sh
keyfork derive openpgp "Storage Key" | sq key extract-cert > storage.pgp
```

The final argument should be a descriptive name for the key, as that name will
likely be used by any program encrypting data to the public key.

The key, including the secret portions, can be retrieved by running the command
without the `sq` portion, but should not be run on a system with a persistent
filesystem, to avoid keeping the key on written memory for longer than
necessary.
