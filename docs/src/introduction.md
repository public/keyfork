{{#include links.md}}

# Introduction

Keyfork is a tool to help manage the creation and derivation of binary data
using [BIP-0039] mnemonics. A mnemonic is, in simple terms, a way of encoding a
large number between 128 and 256 bits, as a list of 12 to 24 words that can be
easily stored or memorized. Once a user has a mnemonic, Keyfork utilizes
[BIP-0032] to derive cryptographic keys, which can be utilized by a variety of
applications.

## Rust documentation

Documentation is [automatically built][keyfork-rustdoc].
