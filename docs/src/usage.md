{{#include links.md}}

# Common Usage

Keyfork is a tool to help manage the creation and derivation of binary data
using [BIP-0039] mnemonics. A mnemonic is, in simple terms, a way of encoding a
large number between 128 and 256 bits, as a list of 12 to 24 words that can be
easily stored or memorized. Once a user has a mnemonic, Keyfork utilizes
[BIP-0032] to derive cryptographic keys, which can be utilized by a variety of
applications.

### Personal Setup

On an airgapped system, run the following command to generate a BIP-0039
mnemonic encoding a generated seed:

```sh
keyfork mnemonic generate
```

Once generated, the mnemonic should be written down and stored in a secure
location such as a safe.

<!--
The default format is to use system entropy, but playing cards, tarot cards, or
dice can be used to generate physical entropy
-->

The Keyfork server can be started by running the following command:

```sh
keyfork recover mnemonic
```

### Group Setup

This guide assumes you are sharding to an `N`-of-`M` system with `I` smart
cards per shardholder. The variables will be used in the following commands as
`$N`, `$M`, and `$I`. The smart cards will be factory reset during the process.

On an airgapped system, run the following command to generate a file containing
encrypted shards of a generated seed:

```sh
keyfork wizard generate-shard-secret --threshold $N --max $M --keys-per-shard $I --output shards.pgp
```

Once generated, the shards file can be safely stored in any location, as the
only information that can be obtained from the shard file is the `$N` value.

If all shardholders are physically present, the Keyfork server can be started
by running the following command:

```sh
keyfork recover shard
```

Otherwise, the Keyfork server can be started by transporting the shards to the
machine using the following command:

```sh
keyfork recover remote-shard
```

Each shard can be transported by running the following command:

```sh
keyfork shard transport shard.pgp
```

### Deriving Keys

Keys can be derived from Keyfork using the `keyfork derive` command, such as
the following command for an OpenPGP certificate with one of each subkey:

```sh
keyfork derive openpgp "John Doe <jdoe@example.com>"
```
