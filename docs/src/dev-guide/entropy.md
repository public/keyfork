{{#include ../links.md}}

# Entropy Guide

Keyfork provides a `keyfork-entropy` crate for generating entropy. The crate
exposes two functions, `ensure_safe` and `generate_entropy_of_size`. The former
function ensures the kernel is of a version with a safe CSPRNG and that no
network interfaces are listed as up, while the latter function reads `size`
bytes from `/dev/urandom` and returns it. This function is used for any crate
where (non-derived) entropy is needed.

Some crates will only accept a `rand::RngCore` type. For this, the `OsRng` RNG
must be used. It is listed as a `CryptoRng` and has been audited to confirm it
calls the `getrandom()` function in a secure manner, calling the function with
zero flags enabled (outside of validating the syscall is available).
