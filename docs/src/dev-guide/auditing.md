{{#include ../links.md}}

# Auditing Dependencies

Dependencies must be reviewed before being added to the repository, and must
not be added for pure convenience. There are few exceptions, such as `clap` and
`thiserror`, which provide derivation macros that are used heavily throughout
`keyfork` and the codebase as a whole. Any dependency added must be reviewed at
least on a surface level to ensure no malicious actions are performed with the
data the library will be responsible for handling. For example, any use of
`std::process` in a crate providing cryptographic functions should be heavily
scrutinized, and any crate that loads arbitrary code or performs networking
requests should have an incredibly important reason for doing so.

Dependencies should be restricted such that the least amount of dead code is
enabled. For instance, a crate such as `keyfork_derive_openpgp` can only make
use of the `ed25519` algorithm, so it exports its own `derive_util` that only
includes the crates required for that library. This can then be used by
programs such as `keyfork-shard`'s OpenPGP mode or `keyfork provision openpgp`
to ensure only the required dependencies are enabled. This reduces the burden
of auditors, but it does mean we can't use projects such as [`hakari`] to
optimize full-project builds.

Below is a list of all immediate dependencies enabled by default, and why they
have been used, as of 2024-01-15.

### Common Dependencies

These dependencies will show up often:

* [`serde`]: Rust-native serialization and deserialization of types.
* [`thiserror`]: Automatic generation of error types.
* [`sequoia-openpgp`]: An OpenPGP interface.

## `keyfork`

A command line interface for generating, deriving from, and managing secrets.

* [`card-backend-pcsc`]: Interacting with smartcards using PCSC. Used as a card
  backend for [`openpgp-card`].
* [`clap`]: Command line argument parsing, helps building an intuitive command
  line interface.
* [`clap_complete`]: Shell autocompletion file generator. Helps the user
  experience. Can be disabled.
* [`keyfork-derive-openpgp`]
* [`keyfork-derive-util`]
* [`keyfork-entropy`]
* [`keyfork-mnemonic`]
* [`keyfork-prompt`]
* [`keyfork-qrcode`]
* [`keyfork-shard`]
* [`keyforkd`]
* [`keyforkd-client`]
* [`openpgp-card`]: Managing OpenPGP smart cards.
* [`openpgp-card-sequoia`]: Managing OpenPGP smart cards with Sequoia OpenPGP.
* [`sequoia-openpgp`]: Manage OpenPGP features across Keyfork crates.
* [`serde`]
* [`smex`]
* [`thiserror`]
* [`tokio`]: Starting and running the Keyfork server.

##  `keyforkd`

A server to handle derivation requests without providing access to the root
seed or close-to-root derivations.

* [`bincode`]: Compact data serialization.
* [`keyfork-derive-path-data`]
* [`keyfork-derive-util`]
* [`keyfork-frame`]
* [`keyfork-mnemonic`]
* [`keyforkd-models`]
* [`serde`]
* [`thiserror`]
* [`tokio`]: An async framework, used for handling many UNIX server clients.
* [`tower`]: Framework traits for providing a Service.
* [`tracing`]: Trace requests made to Keyforkd.
* [`tracing-error`]: Error capture support for tracing.
* [`tracing-subscriber`]: Configure logging and span-capture rules for tracing.

## keyforkd-client

A client for [`keyforkd`].

* [`bincode`]: Compact data serialization.
* [`keyfork-derive-util`]
* [`keyfork-frame`]
* [`keyforkd-models`]
* [`thiserror`]

## `keyforkd-models`

Types used by keyforkd and keyforkd-client

* [`keyfork-derive-util`]
* [`serde`]
* [`thiserror`]

## `keyfork-derive-key`

Derivation of arbitrary data using Keyfork.

* [`keyfork-derive-util`]
* [`keyforkd-client`]
* [`smex`]
* [`thiserror`]

## `keyfork-derive-openpgp`

Derivation of OpenPGP Keys using Keyfork.

* [`anyhow`]: Create errors accepted by Sequoia.
* [`ed25519-dalek`]: Ed25519 key parsing.
* [`keyfork-derive-util`]
* [`keyforkd-client`]
* [`sequoia-openpgp`]: Creation of OpenPGP Keys from provided derived keys.

## `keyfork-derive-path-data`

Guessing possible meanings for BIP-0032 derivation paths.

* [`keyfork-derive-util`]

## `keyfork-derive-util`

BIP-0032 derivation.

* [`digest`]: Hash digest creation.
* [`ed25519-dalek`]: Ed25519 key parsing and arithmetic.
* [`hmac`]: Derivation of keys using HMAC.
* [`k256`]: secp256k1 (K-256) key parsing and arithmetic.
* [`keyfork-mnemonic`]
* [`ripemd`]: Generating hash for fingerprinting of BIP-0032 derived data.
* [`serde`]
* [`sha2`]: Generating hashes for fingerprinting and derivation of data.
* [`thiserror`]

## `keyfork-shard`

M-of-N recombination of secret data using Shamir's Secret Sharing.

* [`aes-gcm`]: Transport encryption for Shamir shares.
* [`anyhow`]: Create errors accepted by Sequoia.
* [`card-backend`]: OpenPGP card backend management.
* [`card-backend-pcsc`]: PCSC support for OpenPGP-card.
* [`hkdf`]: Key derivation for transport encryption keys.
* [`keyfork-derive-openpgp`]
* [`keyfork-mnemonic`]: Encoding encrypted shards using mnemonics.
* [`keyfork-prompt`]
* [`keyfork-qrcode`]: Encoding and decoding of encrypted shards using QR codes.
* [`openpgp-card`]: OpenPGP card support.
* [`openpgp-card-sequoia`]: Sequoia-OpenPGP support for openpgp-card.
* [`sequoia-openpgp`]

## `keyfork-qrcode`

Encoding and decoding of QR codes

* [`image`]: Parse image data, to be sent to rqrr.
* [`rqrr`]: Rust native QR decoding
* [`thiserror`]
* [`v4l`]: Read video frames from a camera device.

## `keyfork-zbar`

ZBar bindings and Rustic interface.

* [`image`]: Transformation from `image::Image` to a ZBar `Image`.
* [`keyfork-zbar-sys`]

## `keyfork-zbar-sys`

Generated bindings for [`zbar`]

### Build dependencies

* [`bindgen`]: Automatic binding generation.
* [`pkg-config`]: Probe for zbar system libary.

## `keyfork-crossterm`

Fork of: https://github.com/rust-lang/pkg-config-rs

## `keyfork-entropy`

Load entropy from a secure source on the current system.

## `keyfork-frame`

Frame data in a length-storing checksum-verified format.

* [`sha2`]: Generate and verify checksum of framed data
* [`thiserror`]
* [`tokio`]: Read and write from AsyncRead and AsyncWrite sources.

## `keyfork-mnemonic`

* [`hmac`]: Hash utilities.
* [`sha2`]: Checksum of mnemonic data and hash for pbkdf2
* [`pbkdf2`]: BIP-0032 seed generation

## `keyfork-prompt`

* [`keyfork-crossterm`]: Interacting with the terminal.
* [`keyfork-mnemonic`]
* [`thiserror`]

## `keyfork-plumbing`

Binaries for `keyfork-entropy` and `keyfork-mnemonic-from-seed`.

* [`keyfork-entropy`]
* [`keyfork-mnemonic`]
* [`smex`]

## `keyfork-slip10-test-data`

Test data for SLIP10/BIP-0032 derivation.

* [`smex`]

## `smex`

Zero-dependency hex encoding and decoding.

[`keyfork-crossterm`]: #keyfork-crossterm
[`keyfork-derive-openpgp`]: #keyfork-derive-openpgp
[`keyfork-derive-path-data`]: #keyfork-derive-path-data
[`keyfork-derive-util`]: #keyfork-derive-util
[`keyfork-entropy`]: #keyfork-entropy
[`keyfork-frame`]: #keyfork-frame
[`keyfork-mnemonic`]: #keyfork-mnemonic
[`keyfork-prompt`]: #keyfork-prompt
[`keyfork-qrcode`]: #keyfork-qrcode
[`keyfork-shard`]: #keyfork-shard
[`keyfork-zbar-sys`]: #keyfork-zbar-sys
[`keyforkd`]: #keyforkd
[`keyforkd-client`]: #keyforkd-client
[`keyforkd-models`]: #keyforkd-models
[`smex`]: #smex
