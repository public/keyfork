{{#include ../../links.md}}

# keyfork-entropy

Retrieve system entropy, output in hex format. The machine must be running a
kernel with BLAKE2 entropy support and not be connected to the Internet.

## Arguments

`keyfork-entropy [size=256]`

* `size`: Number of bits, must be divisible by 8

## Variables

- `INSECURE_HARDWARE_ALLOWED=1`: Bypass system validation
- `SHOOT_SELF_IN_FOOT=1`: Bypass system validation

## Output

Hex-encoded system entropy, of length `size / 4`
