{{#include ../../links.md}}

# keyfork-mnemonic-from-seed

Generate a mnemonic from a seed passed by input.

## Input

Hex-encoded seed, ideally from `keyfork-entropy` or a `keyfork-shard` binary.

## Output

Mnemonic, from 12 to 24 words.
