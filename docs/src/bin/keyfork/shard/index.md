{{#include ../../../links.md}}

# `keyfork shard`

<!-- Linked to: keyfork-user-guide/src/bin/keyfork-shard/index.md -->

Utilities to enable M-of-N sharing of data, using Shamir's Secret Sharing,
supporting multiple formats.

## Options

* `--format`: Either `openpgp` or `p256`, provided anywhere after `split`.
  Keyfork will make a best-effort attempt to automatically guess the format,
  and this flag should not be necessary.

### Format: OpenPGP

The secret is split and shares are automatically encrypted to provided OpenPGP
certificates. The resulting output is an OpenPGP ASCII armored message
containing several sequential encrypted messages.

When decrypting, for any missing keys that do not meet the threshold, the
command will prompt for a Yubikey to provide smartcard-based decryption of
shares.

### Format: p256

This section is incomplete as the functionality for p256 keys is not yet
implemented.

## `keyfork shard split`

Split a secret into threshold-of-max shares.

### Arguments

`keyfork shard split --threshold=threshold --max=max key_discovery`

* `threshold`: Minimum number of operators present to recover the secret
* `max`: Maximum number of operators, must equal the amount of keys in
  `key_discovery`
* `key_discovery`: Either a file or a directory containing public keys.
  If a file, load all public keys from a file.
  If a directory, for every file in the directory (non-recursively), load
  public keys from the file.

### Input

Hex-encoded secret, ideally up to 2048 characters. For larger secrets, encrypt
beforehand using a symmetric key (AES256, for example), and split the symmetric
key.

### Output

The output of the command is dependent on the format.

### Example

```sh
# Export PGP keys of shard holders to key discovery file
gpg --export 88823A75ECAA786B0FF38B148E401478A3FBEF72 F4BF5C81EC78A5DD341C91EEDC4B7D1F52E0BA4D > key_discovery.pgp

# Generate and split a secret
keyfork-entropy | keyfork shard split --format openpgp --threshold 1 --max 2 key_discovery.pgp > shard.pgp
```

## `keyfork shard combine`

Combine `threshold` shares into a secret.

### Arguments

`keyfork shard combine <shard> [key_discovery]`

* `shard`: A file containing the encrypted shards.
* `key_discovery`: Either a file or a directory containing public keys.
  If a file, load all private keys from a file.
  If a directory, for every file in the directory (non-recursively), load
  private keys from the file.
  If the amount of keys found is less than `threshold`, it is up to the format
  to determine how to discover the keys.

### Output

Hex-encoded secret.

### Example

```sh
# Decrypt using only smartcards
keyfork shard combine --format openpgp < shard.pgp | keyfork-mnemonic-from-seed

# Decrypt using on-disk private keys
keyfork shard combine --format openpgp key_discovery.pgp < shard.pgp | keyfork-mnemonic-from-seed
```

## `keyfork shard transport`

Ensure a public key for the shard recipient, decrypt a single shard on the
current system, and prepare it for transport for remote shard recovery.

This command pairs with [`keyfork recover remote-shard`], which should be run
by a remote recovery operator.

### Arguments

`keyfork shard transport <shard> [key_discovery]`

* `shard`: A file containing encrypted shards.
* `key_discovery`: Either a file or a directory containing public keys.
  If a file, load all private keys from a file.
  If a directory, for every file in the directory (non-recursively), load
  private keys from the file.
  If the amount of keys found is less than `threshold`, it is up to the format
  to determine how to discover the keys.

### Prompts

The command will prompt for 33 words from the remote shard recovery operator,
prompt for a smart card to be plugged in, prompt for the PIN for the smart
card, and display a prompt of 72 words to be sent to the remote shard recovery
operator.

### Example

```sh
# Transport using a smart card
keyfork shard transport shard.pgp

# Transport using on-disk private keys
keyfork shard transport key_discovery.pgp shard.pgp
```
