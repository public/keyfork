{{#include ../../links.md}}

# keyfork

The primary interface for interacting with Keyfork utilities.

## `keyfork derive`

Derive keys of various formats. All commands accept an `--account-id` parameter
and may accept additional arguments or optional flags depending on the format
of the derived data. Each format will have its own first-level derivation path,
such as [BIP-0044] or the numeric representation of a textual identifier, such
as "pgp" for OpenPGP keys or "shrd" for Shard OpenPGP keys.

## `keyfork mnemonic`

Utilities pertaining to the creation and management of mnemonics. Mnemonics can
be generated from system entropy or physical entropy, such as a deck of cards
or dice. Once generated, the mnemonic should be stored in a secure location.

## `keyfork shard`

Utilities for splitting secrets to, and combining secrets from, a Shard file.
The Shard file is encrypted at rest, and can be recovered directly by Keyfork
to avoid managing shards or seeds manually.

## `keyfork recover`

Recover a derivation seed and start the Keyfork server. Seeds can be recovered
from mnemonics, a shard file, or any other supported format. Once the seed has
been recovered, the Keyfork server starts, and derivation requests can begin.

## `keyfork wizard`

Utilities to automatically manage the setup of Keyfork. This includes
generating a seed, splitting it into a Shard file, and provisioning smart cards
with the capability to decrypt the shards.
