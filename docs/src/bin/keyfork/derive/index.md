{{#include ../../../links.md}}

# `keyfork derive`

Derive keys of various formats.

## Common Arguments

`keyfork derive <format> --account-id=0`

* `--account-id=0`: The Account ID to append to the derivation request. This
  allows multiple accounts to be derived per format. 

## `keyfork derive openpgp`

Generate an OpenPGP keychain with a Certify, Signing, Encryption, and
Authentication key. The key is generated with a creation date of `1` to ensure
the key fingerprint does not change. The key is set to expire in one day,
encouraging users to establish the ability to renew their key when necessary.
The primary User ID for the certificate is provided as the first argument to
the derivation request, and must contain at least a real name, a username, or
an email, but may contain any combination of the three.

`keyfork derive openpgp "Your Name (Username) <your@email.local>"`
