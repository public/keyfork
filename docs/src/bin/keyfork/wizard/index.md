{{#include ../../../links.md}}

# `keyfork wizard`

Set up Keyfork using a guided setup process.

## `keyfork wizard generate-shard-secret`

Generate a 256-bit seed, derive OpenPGP keys using the seed, provision smart
cards using the derived keys, and export a Shard file.

<div class="warning">
This command will <b>factory-reset</b> any smart cards plugged into the system.
It is recommended to use smart cards dedicated to the purpose of seed recovery.
</div>

### Cryptography Usage

This command makes use of the following cryptographic concepts at the following
steps:

1. 256 bits of entropy are loaded from the system's secure entropy source.
2. The entropy is then converted to a [BIP-0032] seed by applying the
   HmacSha512 function to the entropy. This creates 512 bits of data, where the
   first 256 bits are considered the "seed" and the last 256 bits are
   considered the root key's "chain code".
3. The seed is then derived using BIP-0032 along the path `m / pgp' / shrd' /
   index'`, where the values "pgp" and "shrd" converted to bytes and cast to a
   32 bit integer, and the "index" is a numeric iterator `0..max`. BIP-0032
   uses HmacSha512 with the "chain code" of the previous depth, the private-key
   bytes of the current extended private key, and the index, to derive a new
   extended private key and a new chain code.
4. For each seed, four subkeys are also derived using the above mechanism, and
   converted to an OpenPGP certificate using Ed25519 for signing keys and
   Curve25519 for encryption keys - this transformation is done without any
   modification of the underlying key data, it is a transformation of their
   encoding.
5. The original entropy is then split using Shamir's Secret Sharing configured
   with the values `threshold` and `max`. The share are referred to in
   documentation as "shards".
6. The shards are each concatenated with some metadata (two bytes: the metadata
   version and the `threshold` value) and encrypted to the previously-generated
   Curve25519 OpenPGP key.
7. The OpenPGP certificates are serialized to a vector of bytes and encrypted
   to every shardholder's Curve25519 OpenPGP encryption key.
8. All shards are written to the output file or standard output.

### Arguments

`keyfork wizard generate-shard-secret --threshold=1 --max=3 --keys-per-shard=1 --output=shards.pgp`

* `threshold`: The minimum amount of shardholders needed to recover the
  secret.
* `max`: The maximum amount of shardholders.
* `keys-per-shard`: The amount of smart cardsz to provision per-shardholder.
* `output`: The file to write the generated shard to.

### Prompts

The wizard will prompt for a User PIN and an Admin PIN for every shardholder,
and will require `keys-per-shard` smartcards to be provisioned for every
shardholder.

### Output

An OpenPGP-encrypted Shard file, if not previously configured to be written to
a file using `--output`.
