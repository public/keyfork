{{#include ../../../links.md}}

# `keyfork recover`

Recover a seed to memory from a mnemonic, shard, or other format, then launch
the Keyfork server with the recovered seed.

## `keyfork recover shard`

Decrypt a shard file using keys available on the local system. This command
should be run when all shardholders are present; otherwise, [`keyfork recover
remote-shard`](#keyfork-recover-remote-shard) should be used instead.

### Arguments

`keyfork recover shard <shard_file> [key_discovery]`

* `shard_file`: The file containing the encrypted shards.
* `key_discovery`: The location to find OpenPGP Secret Keys, if not using
  smartcards.

### Prompts

The recovery command will prompt several times during the process, to ensure a
smart card is plugged in, and to request the PIN to unlock the card.

## `keyfork recover remote-shard`

Ensure the transport of transport-encrypted shards and combine them on the
current system. This command should be run when a shardholder is not present;
otherwise, [`keyfork recover shard`](#keyfork-recover-shard) should be used
instead. The command does not take any arguments, as all necessary metadata is
stored encrypted alongside the shard.

The command pairs with [`keyfork shard transport`], which should be run by the
shardholders.

### Prompts

For every shardholder, the recovery command will prompt 33 words to be sent to
the shardholder, followed by an input prompt of 72 words to be received from
the shardholder.
