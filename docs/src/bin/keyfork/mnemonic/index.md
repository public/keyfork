{{#include ../../../links.md}}

# `keyfork mnemonic`

Utilities for managing mnemonics.

## `keyfork mnemonic generate`

Generate a mnemonic using various forms of entropy (default: system). Forms of
entropy also include Tarot cards, playing cards, and dice rolls. Because
ensuring entropy is as random as possible while generating mnemonics, and to
ensure the mnemonic itself is not shared to malicious third parties, Keyfork
requires a system be both offline and running an up-to-date kernel before
generating a mnemonic. However, the command may be run with the variable
`INSECURE_HARDWARE_ALLOWED=1` to override system validation.

### Arguments

`keyfork mnemonic generate --source=system --size=256`

* `--source`: The source from where a seed is created. Can be:
  * `system`

  Choosing any option besides `system` will prompt the user for input:

  * `playing`
  * `tarot`
  * `dice`
* `--size`: The size in bits for the memonic, either `128` or `256`.
