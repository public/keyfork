{{#include ../../links.md}}

# keyfork-shard

<!-- Linked to: keyfork-user-guide/src/bin/keyfork/shard/index.md -->

The `keyfork-shard` crate contains some binaries to enable M-of-N sharing of
data. All binaries use Shamir's Secret Sharing through the [`sharks`] crate.

## OpenPGP

Keyfork provides OpenPGP compatible [`split`][kshard-opgp-split] and
[`combine`][kshard-opgp-combine] versions of Shard binaries. These binaries use
Sequoia OpenPGP and while they require all the necessary certificates for the
splitting stage, the certificates are included in the payload, and once Keyfork
supports decrypting using OpenPGP smartcards, certificates will not be required
to decrypt the shares.
