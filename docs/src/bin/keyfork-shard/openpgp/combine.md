{{#include ../../../links.md}}

# keyfork-shard-combine-openpgp

Combine shares into a previously [`split`][kshard-opgp-split] secret.

## Arguments

`keyfork-shard-combine-openpgp <shard> [key_discovery]`

* `shard`: The shard file to read from.
* `key_discovery`: A file or directory containing OpenPGP keys.
  If the number of keys found is less than `threshold`, an OpenPGP Card
  fallback will be used to decrypt the rest of the messages.

## Pinentry

The terminal may be overridden if the default pinentry command is
`pinentry-curses`, but this will affect neither input nor output.` Pinentry is
used if an OpenPGP key file has an encrypted secret key or to prompt for the
PIN for an OpenPGP smart card.

## Output

Hex-encoded secret.

## Example

```sh
# Decrypt using only smartcards
keyfork-shard-combine-openpgp shard.pgp

# Decrypt using on-disk private keys
keyfork-shard-combine-openpgp key_discovery.pgp shard.pgp
```
