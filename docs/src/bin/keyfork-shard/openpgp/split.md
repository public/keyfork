{{#include ../../../links.md}}

# keyfork-shard-split-openpgp

<!-- Linked to: keyfork-user-guide/src/bin/keyfork-shard/index.md -->

Split a secret into threshold-of-max shares, encrypting each share to an
OpenPGP certificate. The resulting file may be kept by any share operator, but
requires at least `threshold` operators to be present to combine into the
original secret.

## Arguments

`keyfork-shard-split-openpgp threshold max key_discovery`

* `threshold`: Minimum number of operators present to recover the secret
* `max`: Maximum number of operators; this many OpenPGP certs must be available
* `key_discovery`: Either a file or a directory containing OpenPGP certs.
  If a file, load all certificates from the file.
  If a directory, for every file in the directory (non-recursively), load
  certificates from the file.

## Input

Hex-encoded secret, ideally up to 2048 characters. For larger secrets, encrypt
beforehand using a symmetric key (AES256, for example), and split the symmetric
key.

## Output

OpenPGP ASCII armored message containing several sequential encrypted messages.

**Note:** While it is possible to decrypt some of the messages using a tool
like GnuPG or Sequoia, it is not recommended to handle these messages using
tooling outside of Keyfork Shard.

## Example

```sh
# Export PGP keys to key discovery file
gpg --export 88823A75ECAA786B0FF38B148E401478A3FBEF72 F4BF5C81EC78A5DD341C91EEDC4B7D1F52E0BA4D > key_discovery.pgp

# Generate a secret and split
keyfork-entropy | keyfork-shard-split-openpgp 1 2 key_discovery.pgp > shard.pgp
```
