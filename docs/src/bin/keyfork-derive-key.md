{{#include ../links.md}}

# keyfork-derive-key

Derive a key from a given derivation path.

Requires [`keyforkd`] to be running.

## Arguments

`keyfork-derive-key algorithm path`

* `algorithm`: A supported BIP-0032 algorithm, such as `secp256k1` or `ed25519`
* `path`: A BIP-0032 path, such as `m/44'/0'`

It is recommended to use double quotes when writing a derivation path to avoid
the shell silently ignoring the single quotes in the derivation path.

## Output

Hex-encoded private key. Note that this is not the _extended_ private key, and
can't be used to derive further data.
