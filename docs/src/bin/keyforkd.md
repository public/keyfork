{{#include ../links.md}}

# keyforkd

Keyforkd is the backend for deriving data using Keyfork. A mnemonic is loaded
to the server and requests are performed over a UNIX socket. After the server
has validated the request (using whatever approval methods are implemented),
the server will return an extended private key to the client. The extended
private key can then be used either as-is, or to derive further data.

By default, the only validation provided for the request is to ensure the
request contains two indices. By requiring this, `keyforkd` can ensure the
master key is not leaked, and "general" keys (such as `m/44'`, see [BIP-0044])
are not leaked. In the future, `keyforkd` could implement GUI or TTY approval
for users to approve the path requested by the client, such as `m/44'/0'` being
"Bitcoin", or `m/7366512'` being "OpenPGP".

The protocol for the UNIX socket is a framed, [`bincode`] format. While it is
custom to Keyfork, it is easy to implement. The crate `keyfork-frame` provides
a sync (`Read`, `Write`) and Tokio-compatible async (`AsyncRead`, `AsyncWrite`)
pair of methods for encoding and decoding frames.

The payload, binary data, starts with a big-endian u32 length for the rest of
the data. Next, a SHA-256 hash of the remaining data can be decoded. Lastly,
the data itself is stored as-is. Once the data is retrieved, it may be verified
using the previously-loaded SHA-256 hash.

For encoding the data, the process is reversed. A SHA-256 hash is created, and
the length of the hash and the data is encoded to big-endian and written to the
stream. Then, the hash is written to the stream. Lastly, the data itself is
written as-is to the stream.
