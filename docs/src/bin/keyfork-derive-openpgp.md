{{#include ../links.md}}

# keyfork-derive-openpgp

Derive a key from a given derivation path.

Requires [`keyforkd`] to be running.

Because OpenPGP fingerprints are partially based off the time a key was
created, all OpenPGP keys derived using Keyfork use a creation time of UNIX
epoch plus one, to avoid issues of zero being treated as a falsy value.

## Arguments

`keyfork-derive-openpgp path key_format default_userid`

* `path`: A BIP-0032 path, such as `m/7366512'/0'`
* `key_format`: A list of comma-delimited OpenPGP key capabilities; `C` being
  certify (required on the first key), `S` being sign, `E` being both
  encrypt-for-transport and encrypt-for-storage, and `A` being authenticate.
  For OpenPGP cards, an example could be `C,S,E,A`, resulting in one card per
  slot and an off-key certifying key.
* `default_userid`: The default OpenPGP UserID, containing any combination of
  an email, a full name, and a username, such as `"Ryan Heywood (RyanSquared)
  <ryan@distrust.co>"`

It is recommended to use double quotes when writing a derivation path to avoid
the shell silently ignoring the single quotes in the derivation path.

## Output

OpenPGP ASCII armored key, signed to be valid for 24 hours.
