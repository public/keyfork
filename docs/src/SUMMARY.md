<!-- vim:set et sts=0 sw=2 ts=2: -->
{{ #include links.md }}
# Summary

# User Guide

- [Introduction to Keyfork](./introduction.md)
- [Installing Keyfork](./INSTALL.md)
- [Security Considerations](./security.md)
- [Shard Commands](./shard.md)
- [Common Usage](./usage.md)
- [Configuration File](./config-file.md)
- [Binaries](./bin/index.md)
  - [keyfork](./bin/keyfork/index.md)
    - [derive](./bin/keyfork/derive/index.md)
    - [mnemonic](./bin/keyfork/mnemonic/index.md)
    - [shard](./bin/keyfork/shard/index.md)
    - [recover](./bin/keyfork/recover/index.md)
    - [wizard](./bin/keyfork/wizard/index.md)
  - [keyforkd](./bin/keyforkd.md)
  - [keyfork-shard](./bin/keyfork-shard/index.md)
    - [keyfork-shard-split-openpgp](./bin/keyfork-shard/openpgp/split.md)
    - [keyfork-shard-combine-openpgp](./bin/keyfork-shard/openpgp/combine.md)
  - [keyfork-entropy](./bin/keyfork-plumbing/entropy.md)
  - [keyfork-mnemonic-from-seed](./bin/keyfork-plumbing/mnemonic-from-seed.md)
  - [keyfork-derive-key](./bin/keyfork-derive-key.md)
  - [keyfork-derive-openpgp](./bin/keyfork-derive-openpgp.md)

# Developers Guide

- [Handling Data](./dev-guide/handling-data.md)
- [Writing Binaries](./dev-guide/index.md)
  - [Provisioners](./dev-guide/provisioners.md)
- [Auditing Dependencies](./dev-guide/auditing.md)
- [Entropy Guide](./dev-guide/entropy.md)
- [The Shard Protocol](./dev-guide/shard-protocol.md)
