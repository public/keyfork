# Releasing new versions

* Add and review a new blurb to the changelog by running the
`make-changelog-blurb.sh` script and appending the result to the top of
the file.
	* Make sure to add some human-readable snippets at the top!
* Update all versions of crates listed in the changelog.
* Commit changes.
* Run the `sign-new-versions.sh` script to tag the new versions.
* Run the `publish.sh` script to push the latest packages to the Distrust
Cargo registry.
