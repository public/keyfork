set -eu
set -o pipefail

LAST_REF="$1"
CURRENT_REF="${2:-HEAD}"
IGNORE="${3:-ABCDEFG}"

cargo metadata --format-version=1 | \
  jq -r '.packages[] | select(.source == null) | .name + " " + .manifest_path' | \
  while read crate manifest_path; do
    crate_path="$(dirname $manifest_path)"
    git_log="$(git log --format='%h %s' "$LAST_REF".."$CURRENT_REF" "$crate_path" | { grep -v "$IGNORE" || true; })"
    if test ! -z "$git_log"; then
      echo "### Changes in $crate:"
      echo ""
      echo "\`\`\`"
      echo "$git_log"
      echo "\`\`\`"
      echo ""
    fi
done
