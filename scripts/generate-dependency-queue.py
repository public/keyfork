import json
import sys

# pipe `cargo metadata --format-version=1` into this

priority_queue = []
packages = json.load(sys.stdin)["packages"]

def sort_graph(unsorted):
    sorted = []
    while unsorted:
        deadlock = True
        for node, edges in list(unsorted.items()):
            for edge in edges:
                if edge in unsorted:
                    break
            else:
                del unsorted[node]
                sorted.append((node, edges))
                deadlock = False
        if deadlock:
            raise Exception("deadlock")
    return sorted

packages_dict = {
    package["name"]: [
        dep["name"] for dep in package["dependencies"]
        if dep["kind"] is None
    ]
    for package in packages if package["source"] is None
}

# iter_packages("keyfork")
priority_queue = sort_graph(packages_dict.copy())
for key, _ in priority_queue:
    version = next(p["version"] for p in packages if p["name"] == key)
    print(" ".join([key, version]))
