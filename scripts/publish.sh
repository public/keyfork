#!/bin/bash

set -eu
set -o pipefail

scripts_dir="$(dirname "$0")"
python_script="$scripts_dir/generate-dependency-queue.py"
registry_url="https://git.distrust.co/api/packages/public/cargo"
search_url="${registry_url}/api/v1/crates"

cargo metadata --format-version=1 | python3 "$python_script" | while read -r crate version; do
    # Verify the package does not exist
    if test "${crate}" = "keyfork-tests"; then
      continue
    fi
    if ! curl "${search_url}?q=${crate}" 2>/dev/null | jq -e "$(printf '.crates | .[] | select(.name == "%s" and .max_version == "%s")' "$crate" "$version")" >/dev/null; then
      cargo publish --registry distrust -p "$crate"
    fi
done

