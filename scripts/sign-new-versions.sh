#!/bin/bash
set -eu
set -o pipefail

LAST_REF="$1"
CURRENT_REF="${2:-HEAD}"

temp_file="$(mktemp)"

cargo metadata --format-version=1 | jq -r '.packages[] | select(.source == null) | .name + " " + .manifest_path + " " + .version' > "$temp_file"

while read -r crate manifest_path version <&3; do
  crate_path="$(dirname "$manifest_path")"
  git_log="$(git log --format='%h %s' "$LAST_REF".."$CURRENT_REF" "$crate_path")"
  git_tag="$(git tag --list "$crate-v${version}")"
  if test ! -z "$git_log" -a -z "$git_tag"; then
    {
      echo "${crate} v${version}"
      echo ""
      echo "\`\`\`"
      echo "$git_log"
      echo "\`\`\`"
      echo ""
      echo "# Crate: ${crate} ${version}"
    } | git tag --sign "${crate}-v${version}" -F - -e
    reset
    echo "Making new tag: ${crate}-v${version}"
  fi
done 3<"$temp_file"

rm "$temp_file"
