#![allow(missing_docs)]

use std::time::Duration;

use keyfork_qrcode::scan_camera;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let output = scan_camera(Duration::from_secs(15), 0)?;
    if let Some(scanned_text) = output {
        println!("{scanned_text}");
    }

    Ok(())
}
