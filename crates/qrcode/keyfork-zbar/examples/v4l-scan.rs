//! Scan for a barcode or QR code from the default camera.

use std::{
    io::Cursor,
    time::{Duration, SystemTime},
};

use keyfork_zbar::{image::Image, image_scanner::ImageScanner};

use image::io::Reader as ImageReader;
use v4l::{
    buffer::Type,
    io::{traits::CaptureStream, userptr::Stream},
    Device,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let device = Device::new(0)?;

    let mut stream = Stream::with_buffers(&device, Type::VideoCapture, 4)?;
    let start = SystemTime::now();
    let mut scanner = ImageScanner::new();

    while SystemTime::now()
        .duration_since(start)
        .unwrap_or(Duration::from_secs(0))
        < Duration::from_secs(30)
    {
        let (buffer, _) = stream.next()?;
        let image = Image::from(
            ImageReader::new(Cursor::new(buffer))
                .with_guessed_format()?
                .decode()?,
        );

        if let Some(symbol) = scanner.scan_image(&image).first() {
            println!("{}", String::from_utf8_lossy(symbol.data()));
            return Ok(());
        }
    }

    println!("Could not find a QR code");

    Ok(())
}
