//! Rustic bindings to the zbar library. Not intended to be high-level.
//!
//! This library includes a conversion from [`::image::DynamicImage`] for [`image::Image`].
//!
//! Reference: <https://github.com/mchehab/zbar>

pub use keyfork_zbar_sys as sys;

pub use sys::zbar_color_e as Color;
pub use sys::zbar_config_e as Config;
pub use sys::zbar_modifier_e as Modifier;
pub use sys::zbar_orientation_e as Orientation;

pub mod image_scanner;
pub mod image;
pub mod symbol;
