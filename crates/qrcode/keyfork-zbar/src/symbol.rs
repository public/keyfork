//! A Symbol represents some form of encoded data.

use super::sys;

/// The type of symbol (i.e. what type of barcode or QR code).
pub use sys::zbar_symbol_type_e as SymbolType;

// TODO: config, modifiers

/// A Symbol detected by zbar.
#[derive(Debug)]
pub struct Symbol {
    _type: SymbolType,
    data: Vec<u8>,
}

impl Symbol {
    pub(crate) fn new(_type: SymbolType, data: &[u8]) -> Self {
        Self {
            _type,
            data: data.to_vec(),
        }
    }

    /// The type of symbol
    pub fn _type(&self) -> SymbolType {
        self._type
    }

    /// The internal data of the image.
    pub fn data(&self) -> &[u8] {
        self.data.as_slice()
    }

    /// Consume self, returning the internal data.
    pub fn into_data(self) -> Vec<u8> {
        self.data
    }
}
