# Keyfork: The Kitchen Sink of Entropy

**Note:** Keyfork operations are meant to be run on an airgapped machine and
Keyfork will error if either any network interfaces are detected or if Keyfork
is running on a system with a kernel using an insecure random number generator.

An all-inclusive crate encapsulating end-user functionality of the Keyfork
ecosystem, the Keyfork binary includes all mechanisms that should be exposed to
the user when running Keyfork.  Information about what operations Keyfork
performs are available in detail by running `keyfork help` (each subcommand has
thorough documentation) or in the [`docs`] mdBook, but here's a quick overview:

## Getting Started with Keyfork

Keyfork offers two options for getting started. For multi-user setups, it is
best to look at the detailed documentation for Keyfork Shard. For single-user
setups, `keyfork mnemonic generate` will (by default) create a 256-bit mnemonic
phrase that can be used to start Keyfork. *Store this phrase*, as it's the only
way you'll be able to start Keyfork in the future. It is recommended to use a
mnemonic recovery sheet or a printed-steel solution such as the [Billfodl] or
[Cryptosteel Capsule].

```sh
keyfork mnemonic generate
```

Once a mnemonic has been generated and stored in a secure manner, Keyfork can
be started by "recovering" the server from the mnemonic backup mechanism:

```sh
keyfork recover mnemonic
```

## Deriving Keys

Keyfork's primary goal is to derive keys. These keys can later be used for
things such as signing documents and artifacts or decrypting payloads.
Keyfork's first derivation target is OpenPGP, a protocol supporting many
cryptographic operations. OpenPGP keys require a User ID, which can be used to
identify the owner of the key, either by name or by email. To get an OpenPGP
public key (more accurately known as a "cert"), the [`sq`][sq] tool is used to
convert a key to a certificate:

```sh
keyfork derive openpgp "John Doe <jdoe@example.com>" | sq key extract-cert
```

All Keyfork derivations are intended to be reproducible. Because of this,
Keyfork derived keys can be recreated at any time, only requiring the knowledge
of how the key was made.

[`docs`]: /public/keyfork/src/branch/main/docs/src/SUMMARY.md
[Billfodl]: https://privacypros.io/products/the-billfodl/
[Cryptosteel Capsule]: https://cryptosteel.com/product/cryptosteel-capsule-solo/
[sq]: https://gitlab.com/sequoia-pgp/sequoia-sq/
