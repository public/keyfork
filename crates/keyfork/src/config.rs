use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Mnemonic {
    pub hash: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Provisioner {
    pub account: u32,
    pub identifier: String,
    pub metadata: Option<HashMap<String, String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    pub mnemonic: Mnemonic,
    pub provisioner: Vec<Provisioner>,
}
