use super::{
    create,
    derive::{self, Deriver},
    provision,
    Keyfork,
};
use crate::{clap_ext::*, config, openpgp_card::factory_reset_current_card};
use card_backend_pcsc::PcscBackend;
use clap::{builder::PossibleValue, Parser, Subcommand, ValueEnum};
use std::{
    collections::HashMap,
    fmt::Display,
    fs::File,
    io::{IsTerminal, Write},
    path::{Path, PathBuf},
    str::FromStr,
};

use keyfork_derive_openpgp::{
    openpgp::{
        self,
        armor::{Kind, Writer},
        packet::{UserID, signature::SignatureBuilder},
        policy::StandardPolicy,
        serialize::{
            stream::{Encryptor2, LiteralWriter, Message, Recipient},
            Serialize,
        },
        types::{KeyFlags, SignatureType},
    },
    XPrv,
};
use keyfork_derive_util::DerivationIndex;
use keyfork_prompt::{
    default_handler, prompt_validated_passphrase,
    validators::{SecurePinValidator, Validator},
};
use keyfork_shard::{openpgp::OpenPGP, Format};

type StringMap = HashMap<String, String>;

#[derive(Clone, Debug, Default)]
pub enum SeedSize {
    Bits128,

    #[default]
    Bits256,
}

// custom impl to override names in ValueEnum
impl ValueEnum for SeedSize {
    fn value_variants<'a>() -> &'a [Self] {
        &[Self::Bits128, Self::Bits256]
    }

    fn to_possible_value(&self) -> Option<PossibleValue> {
        Some(PossibleValue::new(match self {
            SeedSize::Bits128 => "128",
            SeedSize::Bits256 => "256",
        }))
    }
}

#[derive(thiserror::Error, Debug, Clone)]
pub enum SeedSizeError {
    #[error("Expected one of 128, 256")]
    InvalidChoice,
}

impl Display for SeedSize {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SeedSize::Bits128 => write!(f, "128"),
            SeedSize::Bits256 => write!(f, "256"),
        }
    }
}

impl std::str::FromStr for SeedSize {
    type Err = SeedSizeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "128" => SeedSize::Bits128,
            "256" => SeedSize::Bits256,
            _ => return Err(SeedSizeError::InvalidChoice),
        })
    }
}

impl From<&SeedSize> for usize {
    fn from(value: &SeedSize) -> Self {
        match value {
            SeedSize::Bits128 => 128,
            SeedSize::Bits256 => 256,
        }
    }
}

#[derive(Clone, Debug, thiserror::Error)]
pub enum MnemonicSeedSourceParseError {
    #[error("Expected one of system, playing, tarot, dice")]
    InvalidChoice,
}

#[derive(Clone, Debug, Default, ValueEnum)]
pub enum MnemonicSeedSource {
    /// System entropy
    #[default]
    System,

    /// Playing cards
    Playing,

    /// Tarot cards
    Tarot,

    /// Dice
    Dice,
}

impl std::str::FromStr for MnemonicSeedSource {
    type Err = MnemonicSeedSourceParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "system" => Self::System,
            "playing" => Self::Playing,
            "tarot" => Self::Tarot,
            "dice" => Self::Dice,
            _ => return Err(Self::Err::InvalidChoice),
        })
    }
}

impl MnemonicSeedSource {
    pub fn handle(
        &self,
        size: &SeedSize,
    ) -> Result<keyfork_mnemonic::Mnemonic, Box<dyn std::error::Error>> {
        let size = match size {
            SeedSize::Bits128 => 128,
            SeedSize::Bits256 => 256,
        };
        let seed = match self {
            MnemonicSeedSource::System => keyfork_entropy::generate_entropy_of_size(size / 8)?,
            MnemonicSeedSource::Playing => todo!(),
            MnemonicSeedSource::Tarot => todo!(),
            MnemonicSeedSource::Dice => todo!(),
        };
        let mnemonic = keyfork_mnemonic::Mnemonic::try_from_slice(&seed)?;
        Ok(mnemonic)
    }
}

/// An error occurred while performing an operation.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// An error occurred when interacting iwth a file.
    #[error("Error while performing IO operation on: {1}")]
    IOContext(#[source] std::io::Error, PathBuf),

    /// A required option was not provided.
    #[error("The required option {0} was not provided")]
    MissingOption(&'static str),
}

fn context_stub<'a>(path: &'a Path) -> impl Fn(std::io::Error) -> Error + 'a {
    |e| Error::IOContext(e, path.to_path_buf())
}

#[derive(Subcommand, Clone, Debug)]
pub enum MnemonicSubcommands {
    /// Generate a mnemonic using a given entropy source.
    ///
    /// Mnemonics are a form of encoding a given form of entropy and are used to create a master
    /// seed for BIP-0032 hierarchial derivation. The mnemonic is like the "password" for all of
    /// Keyfork's derivations, and should be treated securely. This command provides a secure
    /// method of generating a seed using system entropy, as well as various forms of loading
    /// physicalized entropy into a mnemonic. The mnemonic should be stored in a safe location
    /// (such as a Trezor "recovery seed card") and never persisted digitally.
    ///
    /// When using the `--shard`, `--shard-to`, `--encrypt-to`, and `--encrypt-to-self` +
    /// `--provision` arguments, the mnemonic is _not_ sent to output. The data for the mnemonic is
    /// then either split using Keyfork Shard or encrypted using OpenPGP.
    Generate {
        /// The source from where a seed is created.
        #[arg(long, value_enum, default_value_t = Default::default())]
        source: MnemonicSeedSource,

        /// The size of the mnemonic, in bits.
        #[arg(long, default_value_t = Default::default())]
        size: SeedSize,

        /// Derive a key. By default, a private key is derived. Unlike other arguments in this
        /// file, arguments must be passed using the format similar to the CLI. For example:
        /// `--derive='openpgp --public "Ryan Heywood <ryan@distrust.co>"'` would be synonymous
        /// with starting the Keyfork daemon with the provided mnemonic, then running
        /// `keyfork derive openpgp --public "Ryan Heywood <ryan@distrust.co>"`.
        ///
        /// The output of the derived key is written to a filename based on the content of the key;
        /// for instance, OpenPGP keys are written to a file identifiable by the certificate's
        /// fingerprint. This behavior can be changed by using the `--to-stdout` or `--output`
        /// modifiers to the `--derive` command.
        #[arg(long)]
        derive: Option<derive::Derive>,

        /// Encrypt the mnemonic to an OpenPGP certificate in the provided path.
        ///
        /// When given arguments in the format `--encrypt-to input.asc,output=output.asc`, the
        /// output of the encryption will be written to `output.asc`. Otherwise, the default
        /// behavior is to write the output to `input.enc.asc`. If the output file already exists,
        /// it will not be overwritten, and the command will exit unsuccessfully.
        #[arg(long)]
        encrypt_to: Option<Vec<ValueWithOptions<PathBuf>>>,

        /// Shard the mnemonic to the certificates in the given Shardfile. Requires a decrypt
        /// operation on the Shardfile to access the metadata and certificates.
        ///
        /// When given arguments in the format `--shard-to input.asc,output=output.asc`, the
        /// output of the encryption will be written to `output.asc`. Otherwise, the default
        /// behavior is to write the output to `input.new.asc`. If the output file already exists,
        /// it will not be overwritten, and the command will exit unsuccessfully.
        #[arg(long)]
        shard_to: Option<Vec<ValueWithOptions<PathBuf>>>,

        /// Shard the mnemonic to the provided certificates.
        ///
        /// The following additional arguments are available:
        ///
        /// * threshold, m: the minimum amount of shares required to reconstitute the shard. By
        ///   default, this is the amount of certificates provided.
        ///
        /// * max, n: the maximum amount of shares. When provided, this is used to ensure the
        ///   certificate count is correct. This is required when using `threshold` or `m`.
        ///
        /// * output: the file to write the generated Shardfile to. By default, assuming the
        ///   certificate input is `input.asc`, the generated Shardfile would be written to
        ///   `input.shard.asc`.
        #[arg(long)]
        shard: Option<Vec<ValueWithOptions<PathBuf>>>,

        /// Encrypt the mnemonic to an OpenPGP certificate derived from the mnemonic, writing the
        /// output to the provided path. This command must be run in combination with
        /// `--provision openpgp-card`, `--derive openpgp`, or another OpenPGP key derivation
        /// mechanism, to ensure the generated mnemonic would be decryptable.
        ///
        /// When used in combination with `--derive` or `--provision` with OpenPGP configurations,
        /// the default behavior is to encrypt the mnemonic to all derived and provisioned
        /// accounts. By default, the account `0` is used.
        #[arg(long)]
        encrypt_to_self: Option<PathBuf>,

        /// Shard the mnemonic to freshly-generated OpenPGP certificates derived from the mnemonic,
        /// writing the output to the provided path, and provisioning OpenPGP smartcards with the
        /// new certificates.
        ///
        /// The following additional arguments are required:
        ///
        /// * threshold, m: the minimum amount of shares required to reconstitute the shard.
        ///
        /// * max, n: the maximum amount of shares.
        ///
        /// * cards_per_shard: the amount of OpenPGP smartcards to provision per shardholder.
        ///
        /// * cert_output: the file to write all generated OpenPGP certificates to; if not
        /// provided, files will be automatically generated for each certificate.
        #[arg(long)]
        shard_to_self: Option<ValueWithOptions<PathBuf>>,

        /// Provision a key derived from the mnemonic to a piece of hardware such as an OpenPGP
        /// smartcard. This argument is required when used with `--encrypt-to-self`.
        ///
        /// Provisioners may choose to output a public key to the current directory by default, but
        /// this functionality may be altered on a by-provisioner basis by providing the `output=`
        /// option to `--provisioner-config`. Additionally, Keyfork may choose to disable
        /// provisioner output if a matching public key has been derived using `--derive`, which
        /// may allow for controlling additional metadata that is not relevant to the provisioned
        /// keys, such as an OpenPGP User ID.
        #[arg(long)]
        provision: Option<provision::Provision>,

        /// The amount of times the provisioner should be run. If provisioning multiple devices at
        /// once, this number should be specified to the number of devices, and all devices should
        /// be plugged into the system at the same time.
        #[arg(long, requires = "provision", default_value = "1")]
        provision_count: usize,

        /// The configuration to pass to the provisioner. These values are specific to each
        /// provisioner, and should be provided in a `key=value,key=value` format. Most
        /// provisioners only expect an `output=` option, to be used in place of the default output
        /// path, if the provisioner needs to write data to a file, such as an OpenPGP certificate.
        #[arg(long, requires = "provision", default_value_t = Options::default())]
        provision_config: Options,
    },
}

// NOTE: This function defaults to `.asc` in the event no extension is found.
// This is specific to OpenPGP. If you want to use this function elsewhere (why?),
// be sure to use a relevant extension for your context.
fn determine_valid_output_path<T: AsRef<Path>>(
    path: &Path,
    mid_ext: &str,
    optional_path: Option<T>,
) -> PathBuf {
    match optional_path {
        Some(p) => p.as_ref().to_path_buf(),
        None => {
            let extension = match path.extension() {
                Some(ext) => format!("{mid_ext}.{ext}", ext = ext.to_string_lossy()),
                None => format!("{mid_ext}.asc"),
            };
            path.with_extension(extension)
        }
    }
}

fn is_extension_armored(path: &Path) -> bool {
    match path.extension().and_then(|s| s.to_str()) {
        Some("pgp") | Some("gpg") => false,
        Some("asc") => true,
        _ => {
            eprintln!("unable to determine whether to armor file: {path:?}");
            eprintln!("use .gpg, .pgp, or .asc extension, or `armor=true`");
            eprintln!("defaulting to armored");
            true
        }
    }
}

fn do_encrypt_to(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    path: &Path,
    options: &StringMap,
) -> Result<(), Box<dyn std::error::Error>> {
    let policy = StandardPolicy::new();

    let output_file = determine_valid_output_path(path, "enc", options.get("output"));

    let is_armored =
        options.get("armor").is_some_and(|a| a == "true") || is_extension_armored(&output_file);

    let certs = OpenPGP::discover_certs(path)?;
    let valid_certs = certs
        .iter()
        .map(|c| c.with_policy(&policy, None))
        .collect::<openpgp::Result<Vec<_>>>()?;
    let recipients = valid_certs.iter().flat_map(|valid_cert| {
        let keys = valid_cert.keys().alive().for_storage_encryption();
        keys.map(|key| Recipient::new(key.keyid(), key.key()))
    });

    let mut output = vec![];
    let message = Message::new(&mut output);
    let encrypted_message = Encryptor2::for_recipients(message, recipients).build()?;
    let mut literal_message = LiteralWriter::new(encrypted_message).build()?;
    literal_message.write_all(mnemonic.to_string().as_bytes())?;
    literal_message.write_all(b"\n")?;
    literal_message.finalize()?;

    let mut file = File::create(&output_file).map_err(context_stub(&output_file))?;
    if is_armored {
        let mut writer = Writer::new(file, Kind::Message)?;
        writer.write_all(&output)?;
        writer.finalize()?;
    } else {
        file.write_all(&output)?;
    }

    Ok(())
}

fn do_encrypt_to_self(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    path: &Path,
    accounts: &[keyfork_derive_util::DerivationIndex],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut certs = vec![];

    for account in accounts.iter().cloned() {
        let userid = UserID::from("Keyfork Temporary Key");

        let subkeys = [
            KeyFlags::empty().set_certification(),
            KeyFlags::empty().set_signing(),
            KeyFlags::empty()
                .set_transport_encryption()
                .set_storage_encryption(),
            KeyFlags::empty().set_authentication(),
        ];

        let seed = mnemonic.generate_seed(None);
        let xprv = XPrv::new(seed)?;
        let derivation_path = keyfork_derive_path_data::paths::OPENPGP
            .clone()
            .chain_push(account);

        let cert =
            keyfork_derive_openpgp::derive(xprv.derive_path(&derivation_path)?, &subkeys, &userid)?;

        certs.push(cert);
    }

    let mut file = tempfile::NamedTempFile::new()?;

    let mut writer = Writer::new(&mut file, Kind::PublicKey)?;
    for cert in certs {
        cert.serialize(&mut writer)?;
    }
    writer.finalize()?;

    let temp_path = file.into_temp_path();

    // a sneaky bit of DRY
    do_encrypt_to(
        mnemonic,
        &temp_path,
        &StringMap::from([(String::from("output"), path.to_string_lossy().to_string())]),
    )?;

    temp_path.close()?;

    Ok(())
}

#[derive(thiserror::Error, Debug)]
#[error("Either the threshold(m) or the max(n) values are missing")]
struct MissingThresholdOrMax;

fn do_shard(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    path: &Path,
    options: &StringMap,
) -> Result<(), Box<dyn std::error::Error>> {
    let output_file = determine_valid_output_path(path, "shard", options.get("output"));

    let is_armored =
        options.get("armor").is_some_and(|a| a == "true") || is_extension_armored(&output_file);

    let threshold = options
        .get("threshold")
        .or_else(|| options.get("m"))
        .map(|s| u8::from_str(s))
        .transpose()?;

    let max = options
        .get("max")
        .or_else(|| options.get("n"))
        .map(|s| u8::from_str(s))
        .transpose()?;

    let certs = OpenPGP::discover_certs(path)?;

    // if neither are set: false
    // if both are set: false
    // if only one is set: true

    if threshold.is_some() ^ max.is_some() {
        return Err(MissingThresholdOrMax)?;
    }

    let (threshold, max) = match threshold.zip(max) {
        Some(t) => t,
        None => {
            let len = u8::try_from(certs.len())?;
            (len, len)
        }
    };

    let openpgp = keyfork_shard::openpgp::OpenPGP;

    let mut output = vec![];
    openpgp.shard_and_encrypt(threshold, max, mnemonic.as_bytes(), &certs[..], &mut output)?;

    let mut file = File::create(&output_file).map_err(context_stub(&output_file))?;
    if is_armored {
        file.write_all(&output)?;
    } else {
        todo!("keyfork does not handle binary shardfiles");
        /*
         * NOTE: this code works, but can't be recombined by Keyfork.
         * therefore, we'll error, before someone tries to use it.
        let mut dearmor = Reader::from_bytes(&output, ReaderMode::Tolerant(None));
        std::io::copy(&mut dearmor, &mut file)?;
        */
    }

    Ok(())
}

fn do_shard_to(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    path: &Path,
    options: &StringMap,
) -> Result<(), Box<dyn std::error::Error>> {
    let output_file = determine_valid_output_path(path, "new", options.get("output"));

    let is_armored =
        options.get("armor").is_some_and(|a| a == "true") || is_extension_armored(&output_file);

    let openpgp = keyfork_shard::openpgp::OpenPGP;
    let prompt = default_handler()?;

    let input = File::open(path)?;
    let (threshold, certs) = openpgp.decrypt_metadata_from_file(
        Some(&[][..]), // the things i must do to avoid qualifying types.
        input,
        prompt,
    )?;

    let mut output = vec![];
    openpgp.shard_and_encrypt(
        threshold,
        u8::try_from(certs.len())?,
        mnemonic.as_bytes(),
        &certs[..],
        &mut output,
    )?;

    let mut file = File::create(&output_file).map_err(context_stub(&output_file))?;
    if is_armored {
        file.write_all(&output)?;
    } else {
        todo!("keyfork does not handle binary shardfiles");
        /*
         * NOTE: this code works, but can't be recombined by Keyfork.
         * therefore, we'll error, before someone tries to use it.
        let mut dearmor = Reader::from_bytes(&output, ReaderMode::Tolerant(None));
        std::io::copy(&mut dearmor, &mut file)?;
        */
    }

    Ok(())
}

fn derive_key(seed: [u8; 64], index: u8) -> Result<openpgp::Cert, Box<dyn std::error::Error>> {
    let subkeys = vec![
        KeyFlags::empty().set_certification(),
        KeyFlags::empty().set_signing(),
        KeyFlags::empty()
            .set_transport_encryption()
            .set_storage_encryption(),
        KeyFlags::empty().set_authentication(),
    ];

    let subkey = DerivationIndex::new(u32::from(index), true)?;
    let path = keyfork_derive_path_data::paths::OPENPGP_SHARD.clone().chain_push(subkey);
    let xprv = XPrv::new(seed)
        .expect("could not construct master key from seed")
        .derive_path(&path)?;
    let userid = UserID::from(format!("Keyfork Shard {index}"));
    let cert = keyfork_derive_openpgp::derive(xprv, &subkeys, &userid)?;
    Ok(cert)
}

fn cross_sign_certs(certs: &mut [openpgp::Cert]) -> Result<(), Box<dyn std::error::Error>> {
    let policy = StandardPolicy::new();

    #[allow(clippy::unnecessary_to_owned)]
    for signing_cert in certs.to_vec() {
        let mut certify_key = signing_cert
            .with_policy(&policy, None)?
            .keys()
            .unencrypted_secret()
            .for_certification()
            .next()
            .expect("certify key unusable/not found")
            .key()
            .clone()
            .into_keypair()?;
        for signable_cert in certs.iter_mut() {
            let sb = SignatureBuilder::new(SignatureType::GenericCertification);
            let userid = signable_cert
                .userids()
                .next()
                .expect("a signable user ID is necessary to create web of trust");
            let signature = sb.sign_userid_binding(
                &mut certify_key,
                signable_cert.primary_key().key(),
                &userid,
            )?;
            let changed;
            (*signable_cert, changed) = signable_cert.clone().insert_packets2(signature)?;
            assert!(
                changed,
                "OpenPGP certificate was unchanged after inserting packets"
            );
        }
    }
    Ok(())
}
fn do_shard_to_self(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    path: &Path,
    options: &StringMap,
) -> Result<(), Box<dyn std::error::Error>> {
    let seed = mnemonic.generate_seed(None);
    let mut pm = default_handler()?;
    let mut certs = vec![];
    let mut seen_cards = std::collections::HashSet::new();

    let threshold: u8 = options
        .get("threshold")
        .or(options.get("m"))
        .ok_or(Error::MissingOption("threshold"))?
        .parse()?;
    let max: u8 = options
        .get("max")
        .or(options.get("n"))
        .ok_or(Error::MissingOption("max"))?
        .parse()?;
    let cards_per_shard = options
        .get("cards_per_shard")
        .as_deref()
        .map(|cps| u8::from_str(cps))
        .transpose()?;

    let pin_validator = SecurePinValidator {
        min_length: Some(8),
        ..Default::default()
    }
    .to_fn();

    for index in 0..max {
        let cert = derive_key(seed, index)?;
        for i in 0..cards_per_shard.unwrap_or(1) {
            pm.prompt_message(keyfork_prompt::Message::Text(format!(
                "Please remove all keys and insert key #{} for user #{}",
                (i as u16) + 1,
                (index as u16) + 1,
            )))?;
            let card_backend = loop {
                if let Some(c) = PcscBackend::cards(None)?.next().transpose()? {
                    break c;
                }
                pm.prompt_message(keyfork_prompt::Message::Text(
                    "No smart card was found. Please plug in a smart card and press enter"
                        .to_string(),
                ))?;
            };
            let pin = prompt_validated_passphrase(
                &mut *pm,
                "Please enter the new smartcard PIN: ",
                3,
                &pin_validator,
            )?;
            factory_reset_current_card(
                &mut |application_identifier| {
                    if seen_cards.contains(&application_identifier) {
                        // we were given a previously-seen card, error
                        // we're gonna panic because this is a significant error
                        panic!("Previously used card {application_identifier} was reused");
                    } else {
                        seen_cards.insert(application_identifier);
                        true
                    }
                },
                pin.trim(),
                pin.trim(),
                &cert,
                &openpgp::policy::NullPolicy::new(),
                card_backend,
            )?;
        }
        certs.push(cert);
    }

    cross_sign_certs(&mut certs)?;

    let opgp = OpenPGP;
    let output = File::create(path)?;
    opgp.shard_and_encrypt(
        threshold,
        certs.len() as u8,
        mnemonic.as_bytes(),
        &certs[..],
        output,
    )?;

    match options.get("cert_output") {
        Some(path) => {
            let cert_file = std::fs::File::create(path)?;
            let mut writer = Writer::new(cert_file, Kind::PublicKey)?;
            for cert in &certs {
                cert.serialize(&mut writer)?;
            }
            writer.finalize()?;
        }
        None => {
            for cert in &certs {
                let path = PathBuf::from(cert.fingerprint().to_string()).with_extension("asc");
                let file = create(&path)?;
                let mut writer = Writer::new(file, Kind::PublicKey)?;
                cert.serialize(&mut writer)?;
                writer.finalize()?;
            }
        }
    }

    Ok(())
}

fn do_provision(
    mnemonic: &keyfork_mnemonic::Mnemonic,
    provision: &provision::Provision,
    count: usize,
    config: &HashMap<String, String>,
) -> Result<(), Box<dyn std::error::Error>> {
    assert!(
        provision.subcommand.is_none(),
        "provisioner was given a subcommand; this functionality is not supported"
    );

    let identifiers = match &provision.identifier {
        Some(identifier) => {
            vec![identifier.clone()]
        }
        None => provision
            .provisioner_name
            .discover()?
            .into_iter()
            .map(|(name, _ctx)| name)
            .collect(),
    };

    assert_eq!(
        identifiers.len(),
        count,
        "amount of provisionable devices discovered did not match provisioner count"
    );

    for identifier in identifiers {
        let provisioner_with_identifier = provision::Provision {
            identifier: Some(identifier),
            ..provision.clone()
        };
        let mut provisioner = config::Provisioner::try_from(provisioner_with_identifier)?;
        match &mut provisioner.metadata {
            Some(metadata) => {
                metadata.extend(config.clone().into_iter());
            }
            metadata @ None => {
                *metadata = Some(config.clone());
            }
        };
        provision
            .provisioner_name
            .provision_with_mnemonic(mnemonic, provisioner)?;
    }

    Ok(())
}

fn do_derive(
    mnemonic: &keyfork_mnemonic::MnemonicBase<keyfork_mnemonic::English>,
    deriver: &derive::Derive,
) -> Result<(), Box<dyn std::error::Error>> {
    let writer = if let Some(output) = deriver.output.as_deref() {
        Some(Box::new(std::fs::File::create(output)?) as Box<dyn Write>)
    } else if deriver.to_stdout {
        Some(Box::new(std::io::stdout()) as Box<dyn Write>)
    } else {
        None
    };
    match deriver {
        derive::Derive {
            command: derive::DeriveSubcommands::OpenPGP(opgp),
            account_id,
            public,
            ..
        } => {
            use keyfork_derive_openpgp::XPrv;
            let root_xprv = XPrv::new(mnemonic.generate_seed(None))?;
            let account = DerivationIndex::new(*account_id, true)?;
            let derived = root_xprv.derive_path(&opgp.derivation_path().chain_push(account))?;
            if *public {
                opgp.derive_public_with_xprv(writer, derived)?;
            } else {
                opgp.derive_with_xprv(writer, derived)?;
            }
        }
        derive::Derive {
            command: derive::DeriveSubcommands::Key(key),
            account_id,
            public,
            ..
        } => {
            // HACK: We're abusing that we use the same key as OpenPGP. Maybe
            // we should use ed25519_dalek.
            use keyfork_derive_openpgp::XPrv;
            let root_xprv = XPrv::new(mnemonic.generate_seed(None))?;
            let account = DerivationIndex::new(*account_id, true)?;
            let derived = root_xprv.derive_path(&key.derivation_path().chain_push(account))?;
            if *public {
                key.derive_public_with_xprv(writer, derived)?;
            } else {
                key.derive_with_xprv(writer, derived)?;
            }
        }
    }
    Ok(())
}

impl MnemonicSubcommands {
    pub fn handle(
        &self,
        _m: &Mnemonic,
        _keyfork: &Keyfork,
    ) -> Result<(), Box<dyn std::error::Error>> {
        match self {
            MnemonicSubcommands::Generate {
                source,
                size,
                derive,
                encrypt_to,
                shard_to,
                shard,
                encrypt_to_self,
                shard_to_self,
                provision,
                provision_count,
                provision_config,
            } => {
                // NOTE: We should never have a case where there's Some() of empty vec, but
                // we will make sure to check it just in case.
                //
                // We do not print the mnemonic if we are:
                // * Encrypting to an existing, usable key
                // * Encrypting to a newly provisioned key
                // * Sharding to an existing Shardfile with usable keys
                // * Sharding to existing, usable keys
                // * Sharding to newly provisioned keys
                let mut will_print_mnemonic =
                    encrypt_to.is_none() || encrypt_to.as_ref().is_some_and(|e| e.is_empty());
                will_print_mnemonic = will_print_mnemonic
                    && (encrypt_to_self.as_ref().is_none() || provision.as_ref().is_none());
                will_print_mnemonic = will_print_mnemonic && shard_to.is_none()
                    || shard_to.as_ref().is_some_and(|s| s.is_empty());
                will_print_mnemonic = will_print_mnemonic && shard.is_none()
                    || shard.as_ref().is_some_and(|s| s.is_empty());
                will_print_mnemonic = will_print_mnemonic && shard_to_self.is_none();

                let mnemonic = source.handle(size)?;

                if let Some(derive) = derive {
                    let stdout = std::io::stdout();
                    if will_print_mnemonic && !stdout.is_terminal() {
                        eprintln!(
                            "Writing plaintext mnemonic and derivation output to standard output"
                        );
                    }
                    do_derive(&mnemonic, derive)?;
                }

                if let Some(encrypt_to) = encrypt_to {
                    for entry in encrypt_to {
                        do_encrypt_to(&mnemonic, &entry.inner, &entry.values)?;
                    }
                }

                if let Some(encrypt_to_self) = encrypt_to_self {
                    let mut accounts: std::collections::HashSet<u32> = Default::default();
                    if let Some(provision::Provision {
                        provisioner_name: provision::Provisioner::OpenPGPCard(_),
                        account_id,
                        ..
                    }) = provision
                    {
                        accounts.insert(*account_id);
                    }
                    if let Some(derive::Derive {
                        command: derive::DeriveSubcommands::OpenPGP(_),
                        account_id,
                        ..
                    }) = derive
                    {
                        accounts.insert(*account_id);
                    }
                    let indices = accounts
                        .into_iter()
                        .map(|i| DerivationIndex::new(i, true))
                        .collect::<Result<Vec<_>, _>>()?;
                    assert!(
                        !indices.is_empty(),
                        "neither derived nor provisioned accounts were found"
                    );
                    do_encrypt_to_self(&mnemonic, &encrypt_to_self, &indices)?;
                }

                if let Some(shard_to_self) = shard_to_self {
                    do_shard_to_self(&mnemonic, &shard_to_self.inner, &shard_to_self.values)?;
                }

                if let Some(provisioner) = provision {
                    // determine if we should write to standard output based on whether we have a
                    // matching pair of provisioner and public derivation output.
                    let mut will_output_public_key = true;

                    if let Some(derive) = derive {
                        let matches = match (provisioner, derive) {
                            (
                                provision::Provision {
                                    provisioner_name: provision::Provisioner::OpenPGPCard(_),
                                    account_id: p_id,
                                    ..
                                },
                                derive::Derive {
                                    command: derive::DeriveSubcommands::OpenPGP(_),
                                    account_id: d_id,
                                    ..
                                },
                            ) => p_id == d_id,
                            _ => false,
                        };
                        if matches && derive.public {
                            will_output_public_key = false;
                        }
                    }

                    let mut values = provision_config.values.clone();
                    if !will_output_public_key && !values.contains_key("output") {
                        values.insert(String::from("_skip_cert_output"), String::from("1"));
                    }

                    do_provision(&mnemonic, provisioner, *provision_count, &values)?;
                }

                if let Some(shard_to) = shard_to {
                    for entry in shard_to {
                        do_shard_to(&mnemonic, &entry.inner, &entry.values)?;
                    }
                }

                if let Some(shard) = shard {
                    for entry in shard {
                        do_shard(&mnemonic, &entry.inner, &entry.values)?;
                    }
                }

                if will_print_mnemonic {
                    println!("{}", mnemonic);
                }
                Ok(())
            }
        }
    }
}

#[derive(Parser, Debug, Clone)]
pub struct Mnemonic {
    #[command(subcommand)]
    pub command: MnemonicSubcommands,
}
