use super::Keyfork;
use crate::config;

use clap::{builder::PossibleValue, Parser, Subcommand, ValueEnum};

use keyfork_derive_util::{DerivationIndex, ExtendedPrivateKey};

mod openpgp;

type Identifier = (String, Option<String>);

#[derive(Debug, Clone)]
pub enum Provisioner {
    OpenPGPCard(openpgp::OpenPGPCard),
    Shard(openpgp::Shard),
}

impl std::fmt::Display for Provisioner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.identifier())
    }
}

impl Provisioner {
    pub fn identifier(&self) -> &'static str {
        match self {
            Provisioner::OpenPGPCard(_) => "openpgp-card",
            Provisioner::Shard(_) => "shard",
        }
    }

    pub fn discover(&self) -> Result<Vec<Identifier>, Box<dyn std::error::Error>> {
        match self {
            Provisioner::OpenPGPCard(o) => o.discover(),
            Provisioner::Shard(s) => s.discover(),
        }
    }

    pub fn provision(
        &self,
        provisioner: config::Provisioner,
    ) -> Result<(), Box<dyn std::error::Error>> {
        match self {
            Provisioner::OpenPGPCard(o) => {
                type Prv = <openpgp::OpenPGPCard as ProvisionExec>::PrivateKey;
                type XPrv = ExtendedPrivateKey<Prv>;
                let account_index = DerivationIndex::new(provisioner.account, true)?;
                let path = <openpgp::OpenPGPCard as ProvisionExec>::derivation_prefix()
                    .chain_push(account_index);
                let mut client = keyforkd_client::Client::discover_socket()?;
                let xprv: XPrv = client.request_xprv(&path)?;
                o.provision(xprv, provisioner)
            }
            Provisioner::Shard(s) => {
                type Prv = <openpgp::Shard as ProvisionExec>::PrivateKey;
                type XPrv = ExtendedPrivateKey<Prv>;
                let account_index = DerivationIndex::new(provisioner.account, true)?;
                let path = <openpgp::Shard as ProvisionExec>::derivation_prefix()
                    .chain_push(account_index);
                let mut client = keyforkd_client::Client::discover_socket()?;
                let xprv: XPrv = client.request_xprv(&path)?;
                s.provision(xprv, provisioner)
            }
        }
    }

    pub fn provision_with_mnemonic(
        &self,
        mnemonic: &keyfork_mnemonic::Mnemonic,
        provisioner: config::Provisioner,
    ) -> Result<(), Box<dyn std::error::Error>> {
        match self {
            Provisioner::OpenPGPCard(o) => {
                type Prv = <openpgp::OpenPGPCard as ProvisionExec>::PrivateKey;
                type XPrv = ExtendedPrivateKey<Prv>;
                let account_index = DerivationIndex::new(provisioner.account, true)?;
                let path = <openpgp::OpenPGPCard as ProvisionExec>::derivation_prefix()
                    .chain_push(account_index);
                let xprv = XPrv::new(mnemonic.generate_seed(None))?.derive_path(&path)?;
                o.provision(xprv, provisioner)
            }
            Provisioner::Shard(s) => {
                type Prv = <openpgp::Shard as ProvisionExec>::PrivateKey;
                type XPrv = ExtendedPrivateKey<Prv>;
                let account_index = DerivationIndex::new(provisioner.account, true)?;
                let path = <openpgp::Shard as ProvisionExec>::derivation_prefix()
                    .chain_push(account_index);
                let xprv = XPrv::new(mnemonic.generate_seed(None))?.derive_path(&path)?;
                s.provision(xprv, provisioner)
            }
        }
    }
}

impl ValueEnum for Provisioner {
    fn value_variants<'a>() -> &'a [Self] {
        &[Self::OpenPGPCard(openpgp::OpenPGPCard), Self::Shard(openpgp::Shard)]
    }

    fn to_possible_value(&self) -> Option<PossibleValue> {
        Some(PossibleValue::new(self.identifier()))
    }
}

#[derive(Debug, thiserror::Error)]
#[error("The given value could not be matched as a provisioner: {0} ({1})")]
pub struct ProvisionerFromStrError(String, String);

impl std::str::FromStr for Provisioner {
    type Err = ProvisionerFromStrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        <Provisioner as ValueEnum>::from_str(s, false)
            .map_err(|e| ProvisionerFromStrError(s.to_string(), e))
    }
}

trait ProvisionExec {
    type PrivateKey: keyfork_derive_util::PrivateKey + Clone;

    /// Discover all known places the formatted key can be deployed to.
    fn discover(&self) -> Result<Vec<Identifier>, Box<dyn std::error::Error>> {
        Ok(vec![])
    }

    /// Return the derivation path for deriving keys.
    fn derivation_prefix() -> keyfork_derive_util::DerivationPath;

    /// Derive a key and deploy it to a target.
    fn provision(
        &self,
        xprv: keyfork_derive_util::ExtendedPrivateKey<Self::PrivateKey>,
        p: config::Provisioner,
    ) -> Result<(), Box<dyn std::error::Error>>;
}

#[derive(Subcommand, Clone, Debug)]
pub enum ProvisionSubcommands {
    /// Discover all available targets on the system.
    Discover,
}

// NOTE: All struct fields are marked as Option so they may be constructed when running a
// subcommand. This is allowed through marking the parent command subcommand_negates_reqs(true).

#[derive(Parser, Debug, Clone)]
pub struct Provision {
    #[command(subcommand)]
    pub subcommand: Option<ProvisionSubcommands>,

    pub provisioner_name: Provisioner,

    /// Account ID.
    #[arg(long, default_value = "0")]
    pub account_id: u32,

    /// Identifier of the hardware to deploy to, listable by running the `discover` subcommand.
    #[arg(long)]
    pub identifier: Option<String>,
}

impl std::str::FromStr for Provision {
    type Err = clap::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Provision::try_parse_from(
            [String::from("provision")]
                .into_iter()
                .chain(shlex::Shlex::new(s)),
        )
    }
}

// NOTE: In the future, this impl will be used by `keyfork recover` to reprovision hardware from
// the config file, iterating through a [Config::provisioner].
// TODO: How can metadata be passed in?

#[derive(thiserror::Error, Debug)]
#[error("Missing field: {0}")]
pub struct MissingField(&'static str);

impl TryFrom<Provision> for config::Provisioner {
    type Error = MissingField;

    fn try_from(value: Provision) -> Result<Self, Self::Error> {
        Ok(Self {
            account: value.account_id,
            identifier: value.identifier.ok_or(MissingField("identifier"))?,
            metadata: Default::default(),
        })
    }
}

impl Provision {
    pub fn handle(&self, _keyfork: &Keyfork) -> Result<(), Box<dyn std::error::Error>> {
        match self.subcommand {
            Some(ProvisionSubcommands::Discover) => {
                let mut iter = self.provisioner_name.discover()?.into_iter().peekable();
                while let Some((identifier, context)) = iter.next() {
                    println!("Identifier: {identifier}");
                    if let Some(context) = context {
                        println!("Context: {context}");
                    }
                    if iter.peek().is_some() {
                        println!("---");
                    }
                }
            }
            None => {
                let provisioner_with_identifier = match self.identifier {
                    Some(_) => self.clone(),
                    None => {
                        let identifiers = self.provisioner_name.discover()?;
                        let [id] = &identifiers[..] else {
                            panic!("invalid amount of identifiers; pass --identifier");
                        };
                        Self {
                            identifier: Some(id.0.clone()),
                            ..self.clone()
                        }
                    }
                };
                let config = config::Provisioner::try_from(provisioner_with_identifier)?;
                self.provisioner_name.provision(config)?;
            }
        }
        Ok(())
    }
}
