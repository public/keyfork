use super::ProvisionExec;
use crate::{
    config,
    openpgp_card::{factory_reset_current_card, get_new_pins},
};

use card_backend_pcsc::PcscBackend;
use keyfork_derive_openpgp::{
    openpgp::{
        armor::{Kind, Writer},
        packet::UserID,
        serialize::Serialize,
        types::KeyFlags,
    },
    XPrv,
};
use keyfork_prompt::default_handler;
use openpgp_card_sequoia::{state::Open, Card};
use std::path::PathBuf;

#[derive(thiserror::Error, Debug)]
#[error("Provisioner was unable to find a matching smartcard")]
struct NoMatchingSmartcard;

fn discover_cards() -> Result<Vec<(String, Option<String>)>, Box<dyn std::error::Error>> {
    let mut idents = vec![];
    for backend in PcscBackend::cards(None)? {
        let backend = backend?;
        let mut card = Card::<Open>::new(backend)?;
        let mut transaction = card.transaction()?;
        let identifier = transaction.application_identifier()?.ident();
        let name = transaction.cardholder_name()?;
        let name = (!name.is_empty()).then_some(name);
        idents.push((identifier, name));
    }
    Ok(idents)
}

fn provision_card(
    provisioner: config::Provisioner,
    xprv: XPrv,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut pm = default_handler()?;

    let (user_pin, admin_pin) = get_new_pins(&mut *pm)?;

    let subkeys = vec![
        KeyFlags::empty().set_certification(),
        KeyFlags::empty().set_signing(),
        KeyFlags::empty()
            .set_transport_encryption()
            .set_storage_encryption(),
        KeyFlags::empty().set_authentication(),
    ];

    let userid = match provisioner.metadata.as_ref().and_then(|m| m.get("userid")) {
        Some(userid) => UserID::from(userid.as_str()),
        None => UserID::from("Keyfork-Provisioned Key"),
    };
    let cert = keyfork_derive_openpgp::derive(xprv.clone(), &subkeys, &userid)?;

    if !provisioner
        .metadata
        .as_ref()
        .is_some_and(|m| m.contains_key("_skip_cert_output"))
    {
        let cert_output = match provisioner.metadata.as_ref().and_then(|m| m.get("output")) {
            Some(cert_output) => PathBuf::from(cert_output),
            None => {
                let path = PathBuf::from(cert.fingerprint().to_string()).with_extension("asc");
                eprintln!(
                    "Writing OpenPGP certificate to: {path}",
                    path = path.display()
                );
                path
            }
        };

        let cert_output_file = std::fs::File::create(cert_output)?;
        let mut writer = Writer::new(cert_output_file, Kind::PublicKey)?;
        cert.serialize(&mut writer)?;
        writer.finalize()?;
    }

    let mut has_provisioned = false;

    for backend in PcscBackend::cards(None)? {
        let backend = backend?;

        let result = factory_reset_current_card(
            &mut |identifier| identifier == provisioner.identifier,
            user_pin.trim(),
            admin_pin.trim(),
            &cert,
            &keyfork_derive_openpgp::openpgp::policy::StandardPolicy::new(),
            backend,
        )?;

        has_provisioned = has_provisioned || result;
    }

    if !has_provisioned {
        return Err(NoMatchingSmartcard)?;
    }

    Ok(())
}

#[derive(Clone, Debug)]
pub struct OpenPGPCard;

impl ProvisionExec for OpenPGPCard {
    type PrivateKey = keyfork_derive_openpgp::XPrvKey;

    fn discover(&self) -> Result<Vec<(String, Option<String>)>, Box<dyn std::error::Error>> {
        discover_cards()
    }

    fn derivation_prefix() -> keyfork_derive_util::DerivationPath {
        keyfork_derive_path_data::paths::OPENPGP.clone()
    }

    fn provision(
        &self,
        xprv: XPrv,
        provisioner: config::Provisioner,
    ) -> Result<(), Box<dyn std::error::Error>> {
        provision_card(provisioner, xprv)
    }
}

#[derive(Clone, Debug)]
pub struct Shard;

impl ProvisionExec for Shard {
    type PrivateKey = keyfork_derive_openpgp::XPrvKey;

    fn discover(&self) -> Result<Vec<(String, Option<String>)>, Box<dyn std::error::Error>> {
        discover_cards()
    }

    fn derivation_prefix() -> keyfork_derive_util::DerivationPath {
        keyfork_derive_path_data::paths::OPENPGP_SHARD.clone()
    }

    fn provision(
        &self,
        xprv: XPrv,
        provisioner: config::Provisioner,
    ) -> Result<(), Box<dyn std::error::Error>> {
        provision_card(provisioner, xprv)
    }
}
