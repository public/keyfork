use super::{Keyfork, create};
use clap::{Args, Parser, Subcommand, ValueEnum};
use std::{fmt::Display, io::Write, path::PathBuf};

use keyfork_derive_openpgp::openpgp::{
    armor::{Kind, Writer},
    packet::UserID,
    serialize::Marshal,
    types::KeyFlags,
    Cert,
};
use keyfork_derive_path_data::paths;
use keyfork_derive_util::{
    request::DerivationAlgorithm, DerivationIndex, DerivationPath, ExtendedPrivateKey as XPrv,
    IndexError, PrivateKey,
};
use keyforkd_client::Client;

type OptWrite = Option<Box<dyn Write>>;

type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

pub trait Deriver {
    type Prv: PrivateKey + Clone;
    const DERIVATION_ALGORITHM: DerivationAlgorithm;

    fn derivation_path(&self) -> DerivationPath;

    fn derive_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()>;

    fn derive_public_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()>;
}

#[derive(Subcommand, Clone, Debug)]
pub enum DeriveSubcommands {
    /// Derive an OpenPGP Transferable Secret Key (private key). The key is encoded using OpenPGP
    /// ASCII Armor, a format usable by most programs using OpenPGP.
    ///
    /// Certificates are created with a default expiration of one day, but may be configured to
    /// expire later using the `KEYFORK_OPENPGP_EXPIRE` environment variable using values such as
    /// "15d" (15 days), "1m" (one month), or "2y" (two years).
    ///
    /// It is recommended to use the default expiration of one day and to change the expiration
    /// using an external utility, to ensure the Certify key is usable.
    #[command(name = "openpgp")]
    OpenPGP(OpenPGP),

    /// Derive an Ed25519 key for a specific algorithm, in a given format.
    Key(Key),
}

/// Derivation path to use when deriving OpenPGP keys.
#[derive(ValueEnum, Clone, Debug, Default)]
pub enum Path {
    /// The default derivation path; no additional index is used.
    #[default]
    Default,

    /// The Disaster Recovery index.
    DisasterRecovery,
}

impl std::fmt::Display for Path {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

impl Path {
    fn as_str(&self) -> &'static str {
        match self {
            Path::Default => "default",
            Path::DisasterRecovery => "disaster-recovery",
        }
    }

    fn derivation_path(&self) -> DerivationPath {
        match self {
            Self::Default => paths::OPENPGP.clone(),
            Self::DisasterRecovery => paths::OPENPGP_DISASTER_RECOVERY.clone(),
        }
    }
}

#[derive(Args, Clone, Debug)]
pub struct OpenPGP {
    /// Default User ID for the certificate, using the OpenPGP User ID format.
    user_id: String,

    /// Derivation path to use when deriving OpenPGP keys.
    #[arg(long, required = false, default_value = "default")]
    derivation_path: Path,
}

/// A format for exporting a key.
#[derive(ValueEnum, Clone, Debug)]
pub enum KeyFormat {
    Hex,
    Base64,
}

/// An invalid slug was provided.
#[derive(thiserror::Error, Debug)]
pub enum InvalidSlug {
    /// The value provided was longer than four bytes.
    #[error("The value provided was longer than four bytes: {0}")]
    InvalidSize(usize),

    /// The value provided was higher than the maximum derivation index.
    #[error("The value provided was higher than the maximum derivation index: {0}")]
    InvalidValue(#[from] IndexError),
}

#[derive(Clone, Debug)]
pub struct Slug(DerivationIndex);

impl std::str::FromStr for Slug {
    type Err = InvalidSlug;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let bytes = s.as_bytes();
        let mut parseable_bytes = [0u8; 4];
        if bytes.len() <= 4 && !bytes.is_empty() {
            parseable_bytes[(4 - bytes.len())..4].copy_from_slice(bytes);
        } else {
            return Err(InvalidSlug::InvalidSize(bytes.len()));
        }
        let slug = u32::from_be_bytes(parseable_bytes);
        let index = DerivationIndex::new(slug, true)?;
        Ok(Slug(index))
    }
}

impl Display for Slug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match (self.0.inner() & (0b1 << 31)).to_be_bytes().as_slice() {
            [0, 0, 0, 0] => Ok(()),
            [0, 0, 0, bytes @ ..] | [0, 0, bytes @ ..] | [0, bytes @ ..] | [bytes @ ..] => f
                .write_str(
                    std::str::from_utf8(&bytes[..]).expect("slug constructed from non-utf8"),
                ),
        }
    }
}

#[derive(Args, Clone, Debug)]
pub struct Key {
    /// The derivation algorithm to derive a key for.
    derivation_algorithm: DerivationAlgorithm,

    /// The output format.
    #[arg(value_enum)]
    format: KeyFormat,

    /// A maximum of four bytes, used for creating the derivation path.
    #[arg(value_parser = clap::value_parser!(Slug))]
    slug: Slug,
}

impl DeriveSubcommands {
    fn handle(&self, account: DerivationIndex, is_public: bool, writer: OptWrite) -> Result<()> {
        match self {
            DeriveSubcommands::OpenPGP(opgp) => {
                let path = opgp.derivation_path();
                let xprv = Client::discover_socket()?
                    .request_xprv::<<OpenPGP as Deriver>::Prv>(&path.chain_push(account))?;
                if is_public {
                    opgp.derive_public_with_xprv(writer, xprv)
                } else {
                    opgp.derive_with_xprv(writer, xprv)
                }
            }
            DeriveSubcommands::Key(key) => {
                let path = key.derivation_path();
                let xprv = Client::discover_socket()?
                    .request_xprv::<<Key as Deriver>::Prv>(&path.chain_push(account))?;
                if is_public {
                    key.derive_public_with_xprv(writer, xprv)
                } else {
                    key.derive_with_xprv(writer, xprv)
                }
            }
        }
    }
}

impl OpenPGP {
    fn cert_from_xprv(&self, xprv: keyfork_derive_openpgp::XPrv) -> Result<Cert> {
        let subkeys = vec![
            KeyFlags::empty().set_certification(),
            KeyFlags::empty().set_signing(),
            KeyFlags::empty()
                .set_transport_encryption()
                .set_storage_encryption(),
            KeyFlags::empty().set_authentication(),
        ];

        let userid = UserID::from(&*self.user_id);
        keyfork_derive_openpgp::derive(xprv, &subkeys, &userid).map_err(Into::into)
    }
}

impl Deriver for OpenPGP {
    type Prv = keyfork_derive_openpgp::XPrvKey;
    const DERIVATION_ALGORITHM: DerivationAlgorithm = DerivationAlgorithm::Ed25519;

    fn derivation_path(&self) -> DerivationPath {
        self.derivation_path.derivation_path()
    }

    fn derive_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()> {
        let cert = self.cert_from_xprv(xprv)?;
        let writer = match writer {
            Some(w) => w,
            None => {
                let path = PathBuf::from(cert.fingerprint().to_string()).with_extension("asc");
                let file = create(&path)?;
                Box::new(file)
            }
        };
        let mut writer = Writer::new(writer, Kind::SecretKey)?;
        for packet in cert.as_tsk().into_packets() {
            packet.serialize(&mut writer)?;
        }
        writer.finalize()?;
        Ok(())
    }

    fn derive_public_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()> {
        let cert = self.cert_from_xprv(xprv)?;
        let writer = match writer {
            Some(w) => w,
            None => {
                let path = PathBuf::from(cert.fingerprint().to_string()).with_extension("asc");
                let file = create(&path)?;
                Box::new(file)
            }
        };
        let mut writer = Writer::new(writer, Kind::PublicKey)?;
        for packet in cert.into_packets2() {
            packet.serialize(&mut writer)?;
        }
        writer.finalize()?;
        Ok(())
    }
}

impl Deriver for Key {
    // HACK: We're abusing that we use the same key as OpenPGP. Maybe we should use ed25519_dalek.
    type Prv = keyfork_derive_openpgp::XPrvKey;
    const DERIVATION_ALGORITHM: DerivationAlgorithm = DerivationAlgorithm::Ed25519;

    fn derivation_path(&self) -> DerivationPath {
        DerivationPath::default().chain_push(self.slug.0.clone())
    }

    fn derive_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()> {
        let (formatted, ext) = match self.format {
            KeyFormat::Hex => (smex::encode(xprv.private_key().to_bytes()), "hex"),
            KeyFormat::Base64 => {
                use base64::prelude::*;
                (BASE64_STANDARD.encode(xprv.private_key().to_bytes()), "b64")
            }
        };
        let filename =
            PathBuf::from(smex::encode(xprv.public_key().to_bytes())).with_extension(ext);
        if let Some(mut writer) = writer {
            writeln!(writer, "{formatted}")?;
        } else {
            std::fs::write(&filename, formatted)?;
        }

        Ok(())
    }

    fn derive_public_with_xprv(&self, writer: OptWrite, xprv: XPrv<Self::Prv>) -> Result<()> {
        let (formatted, ext) = match self.format {
            KeyFormat::Hex => (smex::encode(xprv.public_key().to_bytes()), "hex"),
            KeyFormat::Base64 => {
                use base64::prelude::*;
                (BASE64_STANDARD.encode(xprv.public_key().to_bytes()), "b64")
            }
        };
        let filename =
            PathBuf::from(smex::encode(xprv.public_key().to_bytes())).with_extension(ext);
        if let Some(mut writer) = writer {
            writeln!(writer, "{formatted}")?;
        } else {
            std::fs::write(&filename, formatted)?;
        }
        Ok(())
    }
}

#[derive(Parser, Debug, Clone)]
pub struct Derive {
    #[command(subcommand)]
    pub(crate) command: DeriveSubcommands,

    /// Account ID. Required for all derivations.
    ///
    /// An account ID may not be relevant for the derivation being performed, but the lack of an
    /// account ID can often come as a hindrance in the future. As such, it is always required. If
    /// the account ID is not relevant, it is assumed to be `0`.
    #[arg(long, global = true, default_value = "0")]
    pub(crate) account_id: u32,

    /// Whether derivation should return the public key or a private key.
    #[arg(long, global = true)]
    pub(crate) public: bool,

    /// Whether the file should be written to standard output, or to a filename generated by the
    /// derivation system.
    #[arg(long, global = true, default_value = "false")]
    pub to_stdout: bool,

    /// The file to write the derived public key to, if not standard output. If omitted, a filename
    /// will be generated by the relevant deriver.
    #[arg(long, global = true, conflicts_with = "to_stdout")]
    pub output: Option<PathBuf>,
}

impl Derive {
    pub fn handle(&self, _k: &Keyfork) -> Result<()> {
        let account = DerivationIndex::new(self.account_id, true)?;
        let writer = if let Some(output) = self.output.as_deref() {
            Some(Box::new(std::fs::File::create(output)?) as Box<dyn Write>)
        } else if self.to_stdout {
            Some(Box::new(std::io::stdout()) as Box<dyn Write>)
        } else {
            None
        };
        self.command.handle(account, self.public, writer)
    }
}

impl std::str::FromStr for Derive {
    type Err = clap::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Derive::try_parse_from(
            [String::from("derive")]
                .into_iter()
                .chain(shlex::Shlex::new(s)),
        )
    }
}
