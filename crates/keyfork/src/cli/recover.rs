use super::Keyfork;
use clap::{Parser, Subcommand};
use std::path::PathBuf;
use nix::{
    sys::wait::waitpid,
    unistd::{fork, ForkResult},
};

use keyfork_mnemonic::{English, Mnemonic};
use keyfork_prompt::{
    default_handler, prompt_validated_wordlist,
    validators::{
        mnemonic::{MnemonicChoiceValidator, WordLength},
        Validator,
    },
};
use keyfork_shard::{remote_decrypt, Format};

type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

#[derive(Subcommand, Clone, Debug)]
pub enum RecoverSubcommands {
    /// Decrypt a shard file using keys available on the local system.
    Shard {
        shard_file: PathBuf,
        key_discovery: Option<PathBuf>,
    },

    /// Combine remotely decrypted shards. The shards should be sent using the command `keyfork
    /// shard transport`.
    RemoteShard {},

    /// Read a mnemonic from input.
    Mnemonic {},
}

impl RecoverSubcommands {
    /// Return the 128-bit or 256-bit entropy for a BIP-0039 mnemonic. This is _not_ the same as
    /// the 512-bit seed used by BIP-0032.
    fn handle(&self) -> Result<Vec<u8>> {
        match self {
            RecoverSubcommands::Shard {
                shard_file,
                key_discovery,
            } => {
                let content = std::fs::read_to_string(shard_file)?;
                if content.contains("BEGIN PGP MESSAGE") {
                    let openpgp = keyfork_shard::openpgp::OpenPGP;
                    let prompt_handler = default_handler()?;
                    // TODO: remove .clone() by making handle() consume self
                    let seed = openpgp.decrypt_all_shards_to_secret(
                        key_discovery.as_deref(),
                        content.as_bytes(),
                        prompt_handler,
                    )?;
                    Ok(seed)
                } else {
                    panic!("unknown format of shard file");
                }
            }
            RecoverSubcommands::RemoteShard {} => {
                let mut seed = vec![];
                remote_decrypt(&mut seed)?;
                Ok(seed)
            }
            RecoverSubcommands::Mnemonic {} => {
                let mut prompt_handler = default_handler()?;
                let validator = MnemonicChoiceValidator {
                    word_lengths: [WordLength::Count(12), WordLength::Count(24)],
                };
                let mnemonic = prompt_validated_wordlist::<English, _>(
                    &mut *prompt_handler,
                    "Mnemonic: ",
                    3,
                    &*validator.to_fn(),
                )?;
                Ok(mnemonic.to_bytes())
            }
        }
    }
}

#[derive(Parser, Debug, Clone)]
pub struct Recover {
    #[command(subcommand)]
    command: RecoverSubcommands,

    /// Daemonize the server once started, restoring control back to the shell.
    #[arg(long, global=true)]
    daemon: bool,
}

impl Recover {
    pub fn handle(&self, _k: &Keyfork) -> Result<()> {
        let seed = self.command.handle()?;
        let mnemonic = Mnemonic::try_from_slice(&seed)?;
        if self.daemon {
            // SAFETY: Forking threaded programs is unsafe. We know we don't have multiple
            // threads at this point.
            match unsafe { fork() }? {
                ForkResult::Parent { child } => {
                    // wait for the child to die, so we don't exit prematurely
                    waitpid(Some(child), None)?;
                    return Ok(());
                },
                ForkResult::Child => {
                    if let ForkResult::Parent { .. } = unsafe { fork() }? {
                        return Ok(());
                    }
                },
            }
        }
        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                keyforkd::setup_registry();
                keyforkd::start_and_run_server(mnemonic).await
            })
    }
}
