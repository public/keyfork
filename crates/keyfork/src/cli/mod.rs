use clap::{CommandFactory, Parser, Subcommand};

mod derive;
mod mnemonic;
mod provision;
mod recover;
mod shard;

pub fn create(path: &std::path::Path) -> std::io::Result<std::fs::File> {
    eprintln!("Writing derived key to: {path}", path=path.display());
    std::fs::File::create(path)
}

/// The Kitchen Sink of Entropy.
#[derive(Parser, Clone, Debug)]
#[command(author, version, about, long_about)]
pub struct Keyfork {
    // Global options
    #[command(subcommand)]
    pub command: KeyforkCommands,
}

#[derive(Subcommand, Clone, Debug)]
pub enum KeyforkCommands {
    /// Derive keys of various formats. These commands require that the Keyfork server is running,
    /// which can be started by running a `keyfork recover` command.
    ///
    /// Derived keys are reproducible: assuming the same arguments are used when deriving a key for
    /// a second time, the key will be _functionally_ equivalent. This means keys don't need to be
    /// persisted to cold storage or left hot in a running program. They can be derived when
    /// they're needed and forgotten when they're not.
    Derive(derive::Derive),

    /// Mnemonic generation and persistence utilities.
    Mnemonic(mnemonic::Mnemonic),

    /// Splitting and combining secrets, using Shamir's Secret Sharing.
    ///
    /// Keys can be split such that a certain amount of users, from a potentially even-larger
    /// amount of users, can be used to recreate a key. This creates resilience for a key, as in a
    /// "seven of nine" scenario, nine people in total are capable of recreating a key, but only
    /// seven may be required.
    Shard(shard::Shard),

    /// Derive and deploy keys to hardware.
    ///
    /// Keys existing in hardware creates a situation where it is unlikely (but not impossible) for
    /// a key to be extracted. While a key in memory could be captured by a rootkit or some other
    /// privilege escalation mechanism, a key in hardware would require a hardware exploit to
    /// extract the key.
    ///
    /// It is recommended to provision keys whenever possible, as opposed to deriving them.
    #[command(subcommand_negates_reqs(true))]
    Provision(provision::Provision),

    /// Recover a seed using the requested recovery mechanism and start the Keyfork server.
    ///
    /// Once the Keyfork server is started, derivation requests can be performed. The Keyfork seed
    /// is kept solely in the Keyfork server. Derivations with less than two indices are not
    /// permitted, to ensure a seed often used to derive keys for multiple different paths is not
    /// leaked by any individual deriver.
    Recover(recover::Recover),

    /// Print an autocompletion file to standard output.
    ///
    /// Keyfork does not manage the installation of completion files. Consult the documentation for
    /// the shell for which documentation has been generated on the appropriate location to store
    /// completion files.
    #[cfg(feature = "completion")]
    Completion {
        #[arg(value_enum)]
        shell: clap_complete::Shell,
    },
}

impl KeyforkCommands {
    pub fn handle(&self, keyfork: &Keyfork) -> Result<(), Box<dyn std::error::Error>> {
        match self {
            KeyforkCommands::Derive(d) => {
                d.handle(keyfork)?;
            }
            KeyforkCommands::Mnemonic(m) => {
                m.command.handle(m, keyfork)?;
            }
            KeyforkCommands::Shard(s) => {
                s.command.handle(s, keyfork)?;
            }
            KeyforkCommands::Provision(p) => {
                p.handle(keyfork)?;
            }
            KeyforkCommands::Recover(r) => {
                r.handle(keyfork)?;
            }
            #[cfg(feature = "completion")]
            KeyforkCommands::Completion { shell } => {
                let mut command = Keyfork::command();
                let command_name = command.get_name().to_string();
                clap_complete::generate(
                    *shell,
                    &mut command,
                    command_name,
                    &mut std::io::stdout(),
                );
            }
        }
        Ok(())
    }
}
