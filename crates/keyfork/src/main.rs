#![doc = include_str!("../README.md")]

#![allow(clippy::module_name_repetitions)]

use std::process::ExitCode;

use clap::Parser;

use keyfork_bin::{Bin, ClosureBin};

mod cli;
mod config;
pub mod clap_ext;
mod openpgp_card;

fn main() -> ExitCode {
    let bin = ClosureBin::new(|| {
        let opts = cli::Keyfork::parse();
        opts.command.handle(&opts)
    });

    bin.main()
}
