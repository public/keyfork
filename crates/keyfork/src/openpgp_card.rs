use card_backend_pcsc::PcscBackend;
use keyfork_derive_openpgp::openpgp::{policy::Policy, Cert};
use keyfork_prompt::{
    prompt_validated_passphrase,
    validators::{SecurePinValidator, Validator},
    Message, PromptHandler,
};
use openpgp_card_sequoia::{state::Open, types::KeyType, types::TouchPolicy, Card};

pub fn get_new_pins(
    pm: &mut dyn PromptHandler,
) -> Result<(String, String), Box<dyn std::error::Error>> {
    let user_pin_validator = SecurePinValidator {
        min_length: Some(6),
        ..Default::default()
    }
    .to_fn();
    let admin_pin_validator = SecurePinValidator {
        min_length: Some(8),
        ..Default::default()
    }
    .to_fn();

    let user_pin = loop {
        let user_pin = prompt_validated_passphrase(
            &mut *pm,
            "Please enter the new smartcard User PIN: ",
            3,
            &user_pin_validator,
        )?;
        let validated_user_pin = prompt_validated_passphrase(
            &mut *pm,
            "Please verify the new smartcard User PIN: ",
            3,
            &user_pin_validator,
        )?;
        if user_pin != validated_user_pin {
            pm.prompt_message(Message::Text("User PINs did not match. Retrying.".into()))?;
        } else {
            break user_pin;
        }
    };

    let admin_pin = loop {
        let admin_pin = prompt_validated_passphrase(
            &mut *pm,
            "Please enter the new smartcard Admin PIN: ",
            3,
            &admin_pin_validator,
        )?;
        let validated_admin_pin = prompt_validated_passphrase(
            &mut *pm,
            "Please verify the new smartcard Admin PIN: ",
            3,
            &admin_pin_validator,
        )?;
        if admin_pin != validated_admin_pin {
            pm.prompt_message(Message::Text("Admin PINs did not match. Retrying.".into()))?;
        } else {
            break admin_pin;
        }
    };

    Ok((user_pin, admin_pin))
}

/// Factory reset the current card so long as it does not match the last-used backend.
///
/// The return value of `false` means the filter was matched, whereas `true` means it was
/// successfully provisioned.
pub fn factory_reset_current_card(
    card_filter: &mut dyn FnMut(String) -> bool,
    user_pin: &str,
    admin_pin: &str,
    cert: &Cert,
    policy: &dyn Policy,
    card_backend: PcscBackend,
) -> Result<bool, Box<dyn std::error::Error>> {
    let valid_cert = cert.with_policy(policy, None)?;
    let signing_key = valid_cert
        .keys()
        .for_signing()
        .secret()
        .next()
        .expect("no signing key found");
    let decryption_key = valid_cert
        .keys()
        .for_storage_encryption()
        .secret()
        .next()
        .expect("no decryption key found");
    let authentication_key = valid_cert
        .keys()
        .for_authentication()
        .secret()
        .next()
        .expect("no authentication key found");
    let mut card = Card::<Open>::new(card_backend)?;
    let mut transaction = card.transaction()?;
    let application_identifier = transaction.application_identifier()?.ident();
    if !card_filter(application_identifier) {
        return Ok(false);
    }
    transaction.factory_reset()?;
    let mut admin = transaction.to_admin_card("12345678")?;
    admin.upload_key(signing_key, KeyType::Signing, None)?;
    admin.set_touch_policy(KeyType::Signing, TouchPolicy::On)?;
    admin.upload_key(decryption_key, KeyType::Decryption, None)?;
    admin.set_touch_policy(KeyType::Decryption, TouchPolicy::On)?;
    admin.upload_key(authentication_key, KeyType::Authentication, None)?;
    admin.set_touch_policy(KeyType::Authentication, TouchPolicy::On)?;
    transaction.change_user_pin("123456", user_pin)?;
    transaction.change_admin_pin("12345678", admin_pin)?;
    Ok(true)
}
