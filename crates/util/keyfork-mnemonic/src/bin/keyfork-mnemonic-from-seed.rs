//! Generate a mnemonic from hex-encoded input.

use keyfork_mnemonic::Mnemonic;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::io::stdin();
    let mut line = String::new();
    input.read_line(&mut line)?;
    let decoded = smex::decode(line.trim())?;

    let mnemonic = Mnemonic::from_raw_bytes(&decoded) ;

    println!("{mnemonic}");

    Ok(())
}
