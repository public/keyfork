//! A terminal prompt handler.
//!
//! This prompt handler uses a raw terminal device to read inputs and uses ANSI escape codes to
//! provide formatting for prompts. Because of these reasons, it is not intended to be
//! machine-readable.

use std::{
    borrow::Borrow,
    io::{stderr, stdin, BufRead, BufReader, Read, Stderr, Stdin, Write},
    os::fd::AsRawFd,
};

use keyfork_crossterm::{
    cursor,
    event::{read, DisableBracketedPaste, EnableBracketedPaste, Event, KeyCode, KeyModifiers},
    style::{Print, PrintStyledContent, Stylize},
    terminal::{self, EnterAlternateScreen, FdTerminal, LeaveAlternateScreen, TerminalIoctl},
    tty::IsTty,
    ExecutableCommand, QueueableCommand,
};

use keyfork_bug::bug;

use crate::{BoxResult, Choice, Error, Message, PromptHandler};

#[allow(missing_docs)]
pub type Result<T, E = Error> = std::result::Result<T, E>;

struct TerminalGuard<'a, R, W>
where
    W: Write + AsRawFd,
{
    read: &'a mut BufReader<R>,
    write: &'a mut W,
    terminal: &'a mut FdTerminal,
}

impl<'a, R, W> TerminalGuard<'a, R, W>
where
    W: Write + AsRawFd,
    R: Read,
{
    fn new(read: &'a mut BufReader<R>, write: &'a mut W, terminal: &'a mut FdTerminal) -> Self {
        Self {
            read,
            write,
            terminal,
        }
    }

    fn alternate_screen(mut self) -> std::io::Result<Self> {
        self.execute(EnterAlternateScreen)?;
        Ok(self)
    }

    fn raw_mode(self) -> std::io::Result<Self> {
        self.terminal.enable_raw_mode()?;
        Ok(self)
    }

    fn bracketed_paste(mut self) -> std::io::Result<Self> {
        self.execute(EnableBracketedPaste)?;
        Ok(self)
    }
}

impl<R, W> TerminalIoctl for TerminalGuard<'_, R, W>
where
    R: Read,
    W: Write + AsRawFd,
{
    fn enable_raw_mode(&mut self) -> std::io::Result<()> {
        self.terminal.enable_raw_mode()
    }

    fn disable_raw_mode(&mut self) -> std::io::Result<()> {
        self.terminal.disable_raw_mode()
    }

    fn size(&self) -> std::io::Result<(u16, u16)> {
        self.terminal.size()
    }

    fn window_size(&self) -> std::io::Result<terminal::WindowSize> {
        self.terminal.window_size()
    }
}

impl<R, W> Read for TerminalGuard<'_, R, W>
where
    R: Read,
    W: Write + AsRawFd,
{
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        self.read.read(buf)
    }
}

impl<R, W> BufRead for TerminalGuard<'_, R, W>
where
    R: Read,
    W: Write + AsRawFd,
{
    fn fill_buf(&mut self) -> std::io::Result<&[u8]> {
        self.read.fill_buf()
    }

    fn consume(&mut self, amt: usize) {
        self.read.consume(amt)
    }
}

impl<R, W> Write for TerminalGuard<'_, R, W>
where
    W: Write + AsRawFd,
{
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.write.write(buf)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.write.flush()
    }
}

impl<R, W> Drop for TerminalGuard<'_, R, W>
where
    W: Write + AsRawFd,
{
    fn drop(&mut self) {
        self.write
            .execute(cursor::EnableBlinking)
            .expect(bug!("can't enable blinking"))
            .execute(cursor::Show)
            .expect(bug!("can't show cursor"))
            .execute(DisableBracketedPaste)
            .expect(bug!("can't restore bracketed paste"));
        self.terminal
            .disable_raw_mode()
            .expect(bug!("can't disable raw mode"));
        // we don't want to clear error messages
        if !std::thread::panicking() {
            self.write
                .queue(LeaveAlternateScreen)
                .expect(bug!("can't leave alternate screen"))
                .queue(terminal::Clear(terminal::ClearType::All))
                .expect(bug!("can't clear screen"))
                .queue(cursor::MoveTo(0, 0))
                .expect(bug!("can't move to origin"));
        }
        self.write.flush().expect(bug!("can't execute terminal reset commands"));
    }
}

/// A handler for a terminal.
pub struct Terminal<R, W> {
    read: BufReader<R>,
    write: W,
    terminal: FdTerminal,
}

impl<R, W> Terminal<R, W>
where
    R: Read + Sized,
    W: Write + AsRawFd + Sized,
{
    /// Create a new [`Terminal`] from values implementing [`Read`] and [`Write`].
    ///
    /// # Errors
    /// The function may error if the write handle is not a terminal.
    pub fn new(read_handle: R, write_handle: W) -> Result<Self> {
        if !write_handle.is_tty() {
            return Err(Error::NotATTY);
        }
        Ok(Self {
            read: BufReader::new(read_handle),
            terminal: FdTerminal::from(write_handle.as_raw_fd()),
            write: write_handle,
        })
    }

    fn lock(&mut self) -> TerminalGuard<'_, R, W> {
        TerminalGuard::new(&mut self.read, &mut self.write, &mut self.terminal)
    }
}

impl<R, W> PromptHandler for Terminal<R, W>
where
    R: Read + Sized,
    W: Write + AsRawFd + Sized,
{
    fn prompt_input(&mut self, prompt: &str) -> Result<String> {
        let mut terminal = self.lock().alternate_screen()?.raw_mode()?;
        terminal
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?;
        let mut lines = prompt.lines().peekable();
        let mut prefix_length = 0;
        while let Some(line) = lines.next() {
            prefix_length = line.len();
            terminal.queue(Print(line))?;
            if lines.peek().is_some() {
                terminal.queue(cursor::MoveToNextLine(1))?;
            }
        }
        terminal.flush()?;

        let (mut cols, mut _rows) = terminal.size()?;

        let mut input = String::new();
        loop {
            let input_len = input.len();
            match read()? {
                Event::Resize(new_cols, new_rows) => {
                    cols = new_cols;
                    _rows = new_rows;
                }
                Event::Key(k) => match k.code {
                    KeyCode::Enter => {
                        break;
                    }
                    KeyCode::Backspace => {
                        if input.pop().is_some() && prefix_length + input_len < cols as usize {
                            terminal
                                .queue(cursor::MoveLeft(1))?
                                .queue(Print(" "))?
                                .queue(cursor::MoveLeft(1))?
                                .flush()?;
                        }
                    }
                    KeyCode::Char('w') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        let mut has_deleted_text = true;
                        while input.pop().is_some_and(char::is_whitespace) {
                            has_deleted_text = false;
                        }
                        while input.pop().is_some_and(|c| !c.is_whitespace()) {
                            has_deleted_text = true;
                        }
                        if !input.is_empty() && has_deleted_text {
                            input.push(' ');
                        }
                    }
                    KeyCode::Char('c') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        return Err(Error::CtrlC);
                    }
                    KeyCode::Char(c) => {
                        input.push(c);
                    }
                    _ => (),
                },
                _ => (),
            }

            let printable_start = if prefix_length + input.len() < cols as usize {
                0
            } else {
                let printable_space = (cols as usize) - prefix_length;
                input.len() - (printable_space - 1)
            };
            terminal
                .queue(cursor::MoveToColumn(prefix_length as u16))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
                .queue(Print(&input[printable_start..]))?
                .flush()?;
        }

        Ok(input)
    }

    fn prompt_choice_num(&mut self, prompt: &str, choices: &[Box<dyn Choice>]) -> Result<usize> {
        let mut terminal = self.lock().alternate_screen()?.raw_mode()?;
        terminal
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?
            .queue(cursor::Hide)?;

        for line in prompt.lines() {
            terminal
                .queue(Print(line))?
                .queue(cursor::MoveToNextLine(1))?;

            terminal.flush()?;
        }

        let mut active_choice = 0;
        let mut drawn = false;

        loop {
            let (cols, rows) = terminal.size()?;
            // all choices, plus their padding, plus the spacing between, minus whitespace at end.
            let max_size = choices
                .iter()
                .fold(0usize, |agg, choice| agg + choice.to_string().len() + 2)
                + std::cmp::max(choices.len(), 1)
                - 1;
            let horizontal = max_size < cols.into();
            keyfork_bug::assert!(
                horizontal || usize::from(rows) > prompt.lines().count() + choices.len(),
                "screen too small, can't fit choices on {rows}x{cols}",
            );
            if horizontal {
                terminal.queue(cursor::MoveToColumn(0))?;
            } else if drawn {
                terminal
                    .queue(cursor::MoveUp(
                        choices
                            .len()
                            .saturating_sub(1)
                            .try_into()
                            .expect(keyfork_bug::bug!("more than {} choices provided", u16::MAX)),
                    ))?
                    .queue(cursor::MoveToColumn(0))?;
            } else {
                drawn = true;
            }

            let mut iter = choices.iter().enumerate().peekable();
            while let Some((i, choice)) = iter.next() {
                // if active choice, flip foreground and background
                // if active choice, wrap in []
                // if not, wrap in spaces, to preserve spacing and prevent redraws
                if i == active_choice {
                    terminal.queue(PrintStyledContent(Stylize::reverse(format!("[{choice}]"))))?;
                } else {
                    terminal.queue(Print(format!(" {choice} ")))?;
                }
                if iter.peek().is_some() {
                    if horizontal {
                        terminal.queue(Print(" "))?;
                    } else {
                        terminal.queue(cursor::MoveToNextLine(1))?;
                    }
                }
            }
            terminal.flush()?;

            if let Event::Key(k) = read()? {
                match k.code {
                    KeyCode::Char('c') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        return Err(Error::CtrlC);
                    }
                    KeyCode::Char(c) => {
                        for (i, choice) in choices.iter().enumerate() {
                            if choice.identifier().is_some_and(|id| id == c) {
                                active_choice = i;
                            }
                        }
                    }
                    KeyCode::Left | KeyCode::Up => {
                        active_choice = active_choice.saturating_sub(1);
                    }
                    KeyCode::Right | KeyCode::Down => match choices.len().saturating_sub(active_choice) {
                        0 | 1 => {}
                        _ => {
                            active_choice += 1;
                        }
                    },
                    KeyCode::Enter => {
                        return Ok(active_choice);
                    }
                    _ => {}
                }
            }
        }
    }

    fn prompt_validated_wordlist(
        &mut self,
        prompt: &str,
        retries: u8,
        wordlist: &[&str],
        validator_fn: &mut dyn FnMut(String) -> BoxResult,
    ) -> Result<(), Error> {
        let mut last_error = None;
        for _ in 0..retries {
            let s = self.prompt_wordlist(prompt, wordlist)?;
            match validator_fn(s) {
                Ok(v) => return Ok(v),
                Err(e) => {
                    self.prompt_message(Message::Text(format!("Error validating wordlist: {e}")))?;
                    let _ = last_error.insert(e);
                }
            }
        }
        Err(Error::Validation(
            retries,
            last_error
                .map(|e| e.to_string())
                .unwrap_or_else(|| "Unknown".to_string()),
        ))
    }

    #[allow(clippy::too_many_lines)]
    fn prompt_wordlist(&mut self, prompt: &str, wordlist: &[&str]) -> Result<String> {
        let mut terminal = self
            .lock()
            .alternate_screen()?
            .raw_mode()?
            .bracketed_paste()?;

        terminal
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?;
        let mut lines = prompt.lines().peekable();
        let mut prefix_length = 0;
        while let Some(line) = lines.next() {
            prefix_length = line.len();
            terminal.queue(Print(line))?;
            if lines.peek().is_some() {
                terminal.queue(cursor::MoveToNextLine(1))?;
            }
        }
        terminal.flush()?;

        let (mut cols, mut _rows) = terminal.size()?;
        let mut input = String::new();

        loop {
            match read()? {
                Event::Resize(new_cols, new_rows) => {
                    cols = new_cols;
                    _rows = new_rows;
                }
                Event::Paste(mut p) => {
                    p.retain(|c| c != '\n');
                    input.push_str(&p);
                }
                Event::Key(k) => match k.code {
                    KeyCode::Enter => {
                        break;
                    }
                    KeyCode::Backspace => {
                        input.pop();
                    }
                    KeyCode::Char('w') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        let mut has_deleted_text = true;
                        while input.pop().is_some_and(char::is_whitespace) {
                            has_deleted_text = false;
                        }
                        while input.pop().is_some_and(|c| !c.is_whitespace()) {
                            has_deleted_text = true;
                        }
                        if !input.is_empty() && has_deleted_text {
                            input.push(' ');
                        }
                    }
                    KeyCode::Char('c') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        return Err(Error::CtrlC);
                    }
                    KeyCode::Char(' ') => {
                        if !input.chars().next_back().is_some_and(char::is_whitespace) {
                            input.push(' ');
                        }
                    }
                    KeyCode::Char(c) => {
                        input.push(c);
                        let entry_mode = std::env::var("KEYFORK_PROMPT_MNEMONIC_MODE");
                        if entry_mode.is_ok_and(|mode| mode.to_ascii_lowercase() == "steel") {
                            let word = input.split_whitespace().next_back().map(ToOwned::to_owned);
                            if let Some(steel_word) = word {
                                if steel_word.len() >= 4 {
                                    for word in wordlist.iter().filter(|word| word.len() >= 4) {
                                        if word[..4] == steel_word {
                                            input.push_str(&word[4..]);
                                            input.push(' ');
                                        }
                                    }
                                }
                            }
                        }
                    }
                    _ => (),
                },
                _ => (),
            }

            let usable_space = cols as usize - prefix_length - 1;

            #[allow(clippy::cast_possible_truncation)]
            terminal
                .queue(cursor::MoveToColumn(
                    std::cmp::min(u16::MAX as usize, prefix_length) as u16,
                ))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
                .flush()?;

            let printable_input_start = if input.len() > usable_space {
                let start_index = input.len() - usable_space;
                // Find a word boundary, otherwise slice the current word in half
                if let Some((index, _)) = input
                    .chars()
                    .enumerate()
                    .skip(start_index)
                    .find(|(_, ch)| ch.is_whitespace())
                {
                    index
                } else {
                    start_index
                }
            } else {
                0
            };

            let printable_input = &input[printable_input_start..];
            let mut iter = printable_input.split_whitespace().peekable();

            while let Some(word) = iter.next() {
                if wordlist.contains(&word) {
                    terminal.queue(PrintStyledContent(word.green()))?;
                } else {
                    terminal.queue(PrintStyledContent(word.red()))?;
                }
                if iter.peek().is_some()
                    || printable_input
                        .chars()
                        .next_back()
                        .is_some_and(char::is_whitespace)
                {
                    terminal.queue(Print(" "))?;
                }
            }

            terminal.flush()?;
        }

        Ok(input)
    }

    fn prompt_validated_passphrase(
        &mut self,
        prompt: &str,
        retries: u8,
        validator_fn: &mut dyn FnMut(String) -> BoxResult,
    ) -> Result<(), Error> {
        let mut last_error = None;
        for _ in 0..retries {
            let s = self.prompt_passphrase(prompt)?;
            match validator_fn(s) {
                Ok(v) => return Ok(v),
                Err(e) => {
                    self.prompt_message(Message::Text(format!(
                        "Error validating passphrase: {e}"
                    )))?;
                    let _ = last_error.insert(e);
                }
            }
        }
        Err(Error::Validation(
            retries,
            last_error
                .map(|e| e.to_string())
                .unwrap_or_else(|| "Unknown".to_string()),
        ))
    }

    // TODO: return secrecy::Secret<String>
    fn prompt_passphrase(&mut self, prompt: &str) -> Result<String> {
        let mut terminal = self.lock().alternate_screen()?.raw_mode()?;

        terminal
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?;
        let mut lines = prompt.lines().peekable();
        let mut prefix_length = 0;
        while let Some(line) = lines.next() {
            prefix_length = line.len();
            terminal.queue(Print(line))?;
            if lines.peek().is_some() {
                terminal.queue(cursor::MoveToNextLine(1))?;
            }
        }
        terminal.flush()?;

        let (mut cols, mut _rows) = terminal.size()?;

        let mut passphrase = String::new();
        loop {
            match read()? {
                Event::Resize(new_cols, new_rows) => {
                    cols = new_cols;
                    _rows = new_rows;
                }
                Event::Key(k) => match k.code {
                    KeyCode::Enter => {
                        break;
                    }
                    KeyCode::Backspace => {
                        let passphrase_len = passphrase.len();
                        if passphrase.pop().is_some()
                            && prefix_length + passphrase_len < cols as usize
                        {
                            terminal
                                .queue(cursor::MoveLeft(1))?
                                .queue(Print(" "))?
                                .queue(cursor::MoveLeft(1))?
                                .flush()?;
                        }
                    }
                    KeyCode::Char('c') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        return Err(Error::CtrlC);
                    }
                    KeyCode::Char(c) => {
                        if prefix_length + passphrase.len() < (cols - 1) as usize {
                            terminal.queue(Print("*"))?.flush()?;
                        }
                        passphrase.push(c);
                    }
                    _ => (),
                },
                _ => (),
            }
        }
        Ok(passphrase)
    }

    fn prompt_message(&mut self, prompt: Message) -> Result<()> {
        let mut terminal = self.lock().alternate_screen()?.raw_mode()?;

        loop {
            let (cols, rows) = terminal.size()?;

            terminal
                .queue(terminal::Clear(terminal::ClearType::All))?
                .queue(cursor::MoveTo(0, 0))?;

            match prompt.borrow() {
                Message::Text(text) => {
                    for line in text.lines() {
                        let mut written_chars = 0;
                        for word in line.split_whitespace() {
                            #[allow(clippy::cast_possible_truncation)]
                            let len = std::cmp::min(u16::MAX as usize, word.len()) as u16;
                            written_chars += len + 1;
                            if written_chars > cols {
                                terminal.queue(cursor::MoveToNextLine(1))?;
                                written_chars = len + 1;
                            }
                            terminal.queue(Print(word))?.queue(Print(" "))?;
                        }
                        terminal.queue(cursor::MoveToNextLine(1))?;
                    }
                }
                Message::Data(data) => {
                    let count = data.lines().count();
                    // NOTE: GE to allow a MoveToNextLine(1)
                    if count >= rows as usize {
                        let msg = format!(
                            "{} {count} {} {rows} {}",
                            "Could not print data", "lines long (screen is", "lines long)"
                        );
                        terminal
                            .queue(Print(msg))?
                            .queue(cursor::MoveToNextLine(1))?;
                    } else {
                        for line in data.lines() {
                            terminal
                                .queue(Print(line))?
                                .queue(cursor::MoveToNextLine(1))?;
                        }
                    }
                }
            }
            terminal
                .queue(cursor::DisableBlinking)?
                .queue(PrintStyledContent(" OK ".negative()))?
                .flush()?;

            #[allow(clippy::single_match)]
            match read()? {
                Event::Key(k) => match k.code {
                    KeyCode::Char('c') if k.modifiers.contains(KeyModifiers::CONTROL) => {
                        return Err(Error::CtrlC);
                    }
                    KeyCode::Enter | KeyCode::Char(' ' | 'q') => break,
                    _ => (),
                },
                _ => (),
            }
        }
        Ok(())
    }
}

/// A default terminal, using [`Stdin`] and [`Stderr`].
pub type DefaultTerminal = Terminal<Stdin, Stderr>;

/// Create a [`Terminal`] using the default [`Stdin`] and [`Stderr`] handles.
///
/// # Errors
/// The function may error if [`Stderr`] is not a terminal.
pub fn default_terminal() -> Result<DefaultTerminal> {
    Terminal::new(stdin(), stderr())
}
