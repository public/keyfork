#![allow(missing_docs)]

use keyfork_prompt::default_handler;

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum Choices {
    Retry,
    Continue,
}

impl std::fmt::Display for Choices {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Choices::Retry => write!(
                f,
                "Retry with some really long text that I want to cause issues with."
            ),
            Choices::Continue => write!(
                f,
                "Continue with some really long text that I want to cause issues with."
            ),
        }
    }
}

impl keyfork_prompt::Choice for Choices {
    fn identifier(&self) -> Option<char> {
        Some(match self {
            Choices::Retry => 'r',
            Choices::Continue => 'c',
        })
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut handler = default_handler()?;

    let choice = keyfork_prompt::prompt_choice(
        &mut *handler,
        "Here are some options!",
        &[Choices::Retry, Choices::Continue],
    );

    dbg!(&choice);

    Ok(())
}
