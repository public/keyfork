//! Generate entropy of a given size, encoded as hex.

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let bit_size: usize = std::env::args()
        .nth(1)
        .unwrap_or(String::from("256"))
        .parse()
        .expect("Expected integer bit size");
    assert!(
        bit_size % 8 == 0,
        "Bit size must be divisible by 8, got: {bit_size}"
    );
    match bit_size {
        128 | 256 | 512 => {}
        _ => {
            eprintln!("reading entropy of uncommon size: {bit_size}");
        }
    }

    let entropy = keyfork_entropy::generate_entropy_of_size(bit_size / 8)?;
    println!("{}", smex::encode(entropy));

    Ok(())
}
