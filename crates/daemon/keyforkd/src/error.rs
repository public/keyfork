use thiserror::Error;

/// An error occurred while starting the Keyfork server.
#[derive(Debug, Clone, Error)]
pub enum Keyforkd {
    /// The required environment variables were not set and a socket could not be mounted.
    #[error("Neither KEYFORKD_SOCKET_PATH nor XDG_RUNTIME_DIR were set, nowhere to mount socket")]
    NoSocketPath,
}
