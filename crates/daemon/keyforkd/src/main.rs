//! Launch the Keyfork Server from using a mnemonic passed through standard input.

use keyfork_mnemonic::Mnemonic;

use tokio::io::{self, AsyncBufReadExt, BufReader};

#[cfg(feature = "tracing")]
use tracing::debug;

type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

async fn load_mnemonic() -> Result<Mnemonic> {
    let mut stdin = BufReader::new(io::stdin());
    let mut line = String::new();
    stdin.read_line(&mut line).await?;
    Ok(line.parse()?)
}

#[cfg_attr(feature = "multithread", tokio::main)]
#[cfg_attr(not(feature = "multithread"), tokio::main(flavor = "current_thread"))]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    #[cfg(feature = "tracing")]
    keyforkd::setup_registry();

    #[cfg(feature = "tracing")]
    debug!("reading mnemonic from standard input");
    let mnemonic = load_mnemonic().await?;

    keyforkd::start_and_run_server(mnemonic).await
}
