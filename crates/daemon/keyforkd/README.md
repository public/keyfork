# The Keyfork Server

The server uses a `keyfork_frame`'d `bincode`'d request+response format and can
be interacted with by using the `keyforkd_client` crate.

All requests made to Keyfork are required to list at least two derivation
paths. This helps prevent cases where the master seed or the general protocol
seed are leaked by a client. An example is BIP-0044, where the path used is
`m/44'/0'` for Bitcoin, and often `m/44'/60'` is used for Ethereum. To prevent
an Ethereum wallet from deriving the Bitcoin coin seed, and to prevent leaking
the master seed in general, all requests must contain at least two paths.

Additionally, this ensures that keys are not reused across separate purposes.
Because keys are required to have at least two indexes, they are drawn to the
pattern of using the first index as the key's purpose, such as `m/ pgp'` for
OpenPGP.

## Usage

The Keyfork server can be started by running the `keyfork recover` porcelain
binary or by running `keyforkd` directly and entering a mnemonic. Once started,
the Keyfork server will bind to a socket in `XDG_RUNTIME_DIR` by default, or
`KEYFORK_SOCKET_PATH` pointing directly to where a socket will be bound. Upon
the server's termination, the socket will be removed. The socket will not be
removed by default when the server is starting.

Once started, the Keyfork server can be interacted with using the `keyfork
derive` subcommand or individual `keyfork-derive` commands, such as
`keyfork-derive-openpgp`. The former offers a more intuitive interface, but the
latter may offer a lower dependency surface. These clients will connect to the
server, typically using the `keyforkd-client` crate, and request a derived key.
Once the server has received the derivation request, the server will log the
request, as well as its best-effort guess on what path is being derived (using
the `keyfork-derive-path-data` crate), to inform the user of what keys are
requested. Once the server sends the client the new extended private key, the
client can then choose to use the key as-is, or derive further keys.

## Testing

A Keyfork server can be automatically started by using [`test_util::run_test`].
The function accepts a closure, starting the server before the closure is run,
and closing the server after the closure has completed. This may be useful for
people writing software that interacts with the Keyfork server, such as a
deriver or a provisioner. A test seed must be provided, but can be any content.
The closure accepts one argument, the path of the UNIX socket from which the
server can be accessed.

Examples of the test utility can be seen in the `keyforkd-client` crate.
