use crate::Client;
use keyfork_derive_util::{request::*, DerivationPath};
use keyfork_slip10_test_data::test_data;
use keyforkd::test_util::{run_test, Panicable};
use std::{os::unix::net::UnixStream, str::FromStr};

#[test]
#[cfg(feature = "secp256k1")]
fn secp256k1_test_suite() {
    use k256::SecretKey;

    let tests = test_data()
        .unwrap()
        .remove("secp256k1")
        .unwrap();

    for seed_test in tests {
        let seed = seed_test.seed;
        run_test(&seed, move |socket_path| -> Result<(), Box<dyn std::error::Error + Send>> {
            for test in seed_test.tests {
                let socket = UnixStream::connect(socket_path).unwrap();
                let mut client = Client::new(socket);
                let chain = DerivationPath::from_str(test.chain).unwrap();
                let chain_len = chain.len();
                if chain_len < 2 {
                    continue;
                }
                if chain.iter().take(2).any(|index| !index.is_hardened()) {
                    continue;
                }
                // Consistency check: ensure the server and the client can each derive the same
                // key using an XPrv, for all but the last XPrv, which is verified after this
                for i in 2..chain_len {
                    // FIXME: Keyfork will only allow one request per session
                    let socket = UnixStream::connect(socket_path).unwrap();
                    let mut client = Client::new(socket);
                    let path = DerivationPath::from_str(test.chain).unwrap();
                    let left_path = path.inner()[..i]
                        .iter()
                        .fold(DerivationPath::default(), |p, i| p.chain_push(i.clone()));
                    let right_path = path.inner()[i..]
                        .iter()
                        .fold(DerivationPath::default(), |p, i| p.chain_push(i.clone()));
                    let xprv = dbg!(client.request_xprv::<SecretKey>(&left_path)).unwrap();
                    let derived_xprv = xprv.derive_path(&right_path).unwrap();
                    let socket = UnixStream::connect(socket_path).unwrap();
                    let mut client = Client::new(socket);
                    let keyforkd_xprv = client.request_xprv::<SecretKey>(&path).unwrap();
                    assert_eq!(
                        derived_xprv, keyforkd_xprv,
                        "{left_path} + {right_path} != {path}"
                    );
                }
                let req = DerivationRequest::new(
                    DerivationAlgorithm::Secp256k1,
                    &DerivationPath::from_str(test.chain).unwrap(),
                );
                let response =
                    DerivationResponse::try_from(client.request(&req.into()).unwrap()).unwrap();
                assert_eq!(&response.data, test.private_key.as_slice());
            }
            Ok(())
        })
        .unwrap();
    }
}

#[test]
#[cfg(feature = "ed25519")]
fn ed25519_test_suite() {
    use ed25519_dalek::SigningKey;

    let tests = test_data().unwrap().remove("ed25519").unwrap();

    for seed_test in tests {
        let seed = seed_test.seed;
        run_test(&seed, move |socket_path| {
            for test in seed_test.tests {
                let socket = UnixStream::connect(socket_path).unwrap();
                let mut client = Client::new(socket);
                let chain = DerivationPath::from_str(test.chain).unwrap();
                let chain_len = chain.len();
                if chain_len < 2 {
                    continue;
                }
                for i in 2..chain_len {
                    // Consistency check: ensure the server and the client can each derive the same
                    // key using an XPrv, for all but the last XPrv, which is verified after this
                    let path = DerivationPath::from_str(test.chain).unwrap();
                    let left_path = path.inner()[..i]
                        .iter()
                        .fold(DerivationPath::default(), |p, i| p.chain_push(i.clone()));
                    let right_path = path.inner()[i..]
                        .iter()
                        .fold(DerivationPath::default(), |p, i| p.chain_push(i.clone()));
                    let xprv = dbg!(client.request_xprv::<SigningKey>(&left_path)).unwrap();
                    let derived_xprv = xprv.derive_path(&right_path).unwrap();
                    let keyforkd_xprv = client.request_xprv::<SigningKey>(&path).unwrap();
                    assert_eq!(
                        derived_xprv, keyforkd_xprv,
                        "{left_path} + {right_path} != {path}"
                    );
                }
                let req = DerivationRequest::new(
                    DerivationAlgorithm::Ed25519,
                    &DerivationPath::from_str(test.chain).unwrap(),
                );
                let response =
                    DerivationResponse::try_from(client.request(&req.into()).unwrap()).unwrap();
                assert_eq!(&response.data, test.private_key.as_slice());
            }
            Panicable::Ok(())
        })
        .unwrap();
    }
}
