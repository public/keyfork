//! Combine OpenPGP shards using remote transport and output the hex-encoded secret.

use std::{
    env,
    process::ExitCode,
};

use keyfork_shard::remote_decrypt;

type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

fn run() -> Result<()> {
    let mut args = env::args();
    let program_name = args.next().expect("program name");
    let args = args.collect::<Vec<_>>();
    match args.as_slice() {
        [] => (),
        _ => panic!("Usage: {program_name}"),
    };

    let mut bytes = vec![];
    remote_decrypt(&mut bytes)?;
    print!("{}", smex::encode(bytes));

    Ok(())
}

fn main() -> ExitCode {
    let result = run();
    if let Err(e) = result {
        eprintln!("Error: {e}");
        let mut source = e.source();
        while let Some(new_error) = source.take() {
            eprintln!("Source: {new_error}");
            source = new_error.source();
        }
        return ExitCode::FAILURE;
    }
    ExitCode::SUCCESS
}
