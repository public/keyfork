# Keyfork Shard

Securing secrets using Shamir's Secret Sharing, an "M-of-N" secret recovery
mechanism used to split a secret into `n` encrypted parts, with `m` parts
required to restore the secret.

## Shardfile Formats

Currently, OpenPGP is the only supported format. Any mix of smartcards and
OpenPGP key files are supported.

## Metadata

Keyfork Shard stores some additional metadata inside the Shardfile to make
recombining secrets easier. This metadata currently includes the metadata
version (1) and the threshold required to recreate the secret (meaning you
don't need to remember the threshold!).

## Command Line Usage

The command to run to split and combine a secret is format-dependent, but will
often follow the format `keyfork-shard-split-<format>` and
`keyfork-shard-combine-<format>`. For this example, OpenPGP will be used, but
the flow will be similar for any format. Keyfork Shard expects the input to be
a hex-encoded secret.

```sh
# Read our secret into a shell variable.
read secret

# Shard our secret.
echo $secret | keyfork-shard-split-openpgp 3 5 keyring.pgp > shards.pgp

# Forget our secret.
unset secret

# Recreate our secret. Without specifying a keyring, we are prompted to use
# smartcards.
keyfork-shard-combine-openpgp shards.pgp
```
