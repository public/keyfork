# Keyfork Derive: BIP-0032 Key Derivation

Keyfork offers a [BIP-0032] based hierarchial key derivation system enabling
the ability to create keys based on a [BIP-0032] seed, a value between 128 to
512 bits. The keys can be made using any algorithm supported by Keyfork Derive.
Newtypes can be added to wrap around foreign key types that aren't supported by
Keyfork.

Keys derived with the same parameters, from the same seed, will _always_ return
the same value. This makes Keyfork a reliable backend for generating encryption
or signature keys, as every key can be recovered using the previously used
derivation algorithm. However, this may be seen as a concern, as all an
attacker may need to recreate all previously-used seeds would be the original
derivation seed. For this reason, it is recommended to use the Keyfork server
for derivation from the root seed. The Keyfork server will ensure the root seed
and any highest-level keys (such as BIP-44, BIP-85, etc.) keys are not leaked.

The primary use case of Keyfork Derive will be the creation of Derivation
Requests, to be used by Keyforkd Client. In the included example, derivation is
performed directly on a master seed. This is how Keyforkd works internally.

## Example

```rust
use std::str::FromStr;
use keyfork_mnemonic::Mnemonic;
use keyfork_derive_util::{*, request::*};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mnemonic = Mnemonic::from_str(
        "enter settle kiwi high shift absorb protect sword talent museum lazy okay"
    )?;
    let path = DerivationPath::from_str("m/44'/0'/0'/0/0")?;

    let request = DerivationRequest::new(
        DerivationAlgorithm::Secp256k1,
        &path
    );

    let key1 = request.derive_with_mnemonic(&mnemonic)?;

    let seed = mnemonic.seed(None)?;
    let key2 = request.derive_with_master_seed(&seed)?;

    assert_eq!(key1, key2);

    Ok(())
}
```

[BIP-0032]: https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
