#![allow(clippy::module_name_repetitions, clippy::must_use_candidate)]
#![doc = include_str!("../README.md")]

pub mod extended_key;
pub mod request;

mod index;
mod path;

#[doc(hidden)]
pub mod private_key;

#[doc(hidden)]
pub mod public_key;

#[cfg(test)]
mod tests;

#[doc(inline)]
pub use crate::extended_key::{private_key::{ExtendedPrivateKey, Error as XPrvError, VariableLengthSeed}, public_key::{ExtendedPublicKey, Error as XPubError}};

pub use crate::{
    index::{DerivationIndex, Error as IndexError},
    path::{DerivationPath, Error as PathError},
    private_key::{PrivateKey, PrivateKeyError},
    public_key::{PublicKey, PublicKeyError},
};
