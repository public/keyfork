//! # Extended Key Derivation
//!
//! Oftentimes, a client will want to create multiple keys. Some examples may include deriving
//! non-hardened public keys to see how many wallets have been used, deriving multiple OpenPGP
//! keys, or generally avoiding key reuse. While Keyforkd locks the root mnemonic and the
//! first-level derivation, any second-level derivations acquired from Keyforkd (for example,
//! `"m/44'/0'"`) can be used to derive further keys by converting the key to an Extended Public
//! Key or Extended Private Key and calling [`ExtendedPublicKey::derive_child`] or
//! [`ExtendedPrivateKey::derive_child`].
//!
//! # Examples
//!  ```rust
//! use std::str::FromStr;
//! use keyfork_mnemonic::Mnemonic;
//! use keyfork_derive_util::{*, request::*};
//! use k256::SecretKey;
//!
//! # fn check_wallet<T: PublicKey>(_: ExtendedPublicKey<T>) -> Result<(), Box<dyn std::error::Error>> { Ok(()) }
//! fn main() -> Result<(), Box<dyn std::error::Error>> {
//! #   let mnemonic = Mnemonic::from_str(
//! #       "enter settle kiwi high shift absorb protect sword talent museum lazy okay"
//! #   )?;
//!     let path = DerivationPath::from_str("m/44'/0'/0'/0")?;
//!     let request = DerivationRequest::new(
//!         DerivationAlgorithm::Secp256k1, // The algorithm of k256::SecretKey
//!         &path,
//!     );
//!
//!     let response = // perform a Keyforkd Client request...
//! #       request.derive_with_mnemonic(&mnemonic)?;
//!     let key: ExtendedPrivateKey<SecretKey> = response.try_into()?;
//!     let pubkey = key.extended_public_key();
//!     drop(key);
//!
//!     for account in (0..20).map(|i| DerivationIndex::new(i, false).unwrap()) {
//!         let derived_key = pubkey.derive_child(&account)?;
//!         check_wallet(derived_key);
//!     }
//!
//!     Ok(())
//! }
//! ```

#[allow(missing_docs)]
pub mod private_key;
#[allow(missing_docs)]
pub mod public_key;

pub use {private_key::ExtendedPrivateKey, public_key::ExtendedPublicKey};
