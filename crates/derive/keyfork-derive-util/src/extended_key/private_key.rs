use crate::{DerivationIndex, DerivationPath, ExtendedPublicKey, PrivateKey, PublicKey};

use keyfork_bug::bug;

use hmac::{Hmac, Mac};
use serde::{Deserialize, Serialize};
use sha2::Sha512;
use thiserror::Error;

const KEY_SIZE: usize = 256;

/// Errors associated with creating or deriving Extended Private Keys.
#[derive(Error, Clone, Debug)]
pub enum Error {
    /// The maximum depth for key derivation has been reached. The supported maximum depth is 255.
    #[error("Reached maximum depth for key derivation")]
    Depth,

    /// An unknown error occurred while deriving a child key.
    #[error("Unknown error while deriving child key")]
    Derivation,

    /// The algorithm used mandates hardened derivation only.
    #[error("The algorithm used mandates hardened derivation only")]
    HardenedDerivationRequired,

    /// The given slice was of an inappropriate size to create a Private Key.
    #[error("The given slice was of an inappropriate size to create a Private Key")]
    InvalidSliceError(#[from] std::array::TryFromSliceError),

    /// The given data was not a valid key for the chosen key type.
    #[error("The given data was not a valid key for the chosen key type")]
    InvalidKey,
}

type Result<T, E = Error> = std::result::Result<T, E>;
type ChainCode = [u8; 32];
type HmacSha512 = Hmac<Sha512>;

/// A reference to a variable-length seed. Keyfork automatically supports a seed of 128 bits,
/// 256 bits, or 512 bits, but because the master key is derived from a hashed seed, in theory
/// any amount of bytes could be used. It is not advised to use a variable-length seed longer
/// than 256 bits, as a brute-force attack on the master key could be performed in 2^256
/// attempts.
///
/// Mnemonics use a 512 bit seed, as knowledge of the mnemonics' words (such as through a side
/// channel attack) could leak which individual word is used, but not the order the words are
/// used in. Using a 512 bit hash to generate the seed results in a more computationally
/// expensive brute-force requirement.
pub struct VariableLengthSeed<'a> {
    seed: &'a [u8],
}

impl<'a> VariableLengthSeed<'a> {
    /// Create a new VariableLengthSeed.
    ///
    /// # Examples
    /// ```rust
    /// use sha2::{Sha256, Digest};
    /// use keyfork_derive_util::VariableLengthSeed;
    ///
    /// let data = b"the missile is very eepy and wants to take a small sleeb";
    /// let seed = VariableLengthSeed::new(data);
    /// ```
    pub fn new(seed: &'a [u8]) -> Self {
        Self { seed }
    }
}

mod as_private_key {
    use super::VariableLengthSeed;

    pub trait AsPrivateKey {
        fn as_private_key(&self) -> &[u8];
    }

    impl AsPrivateKey for [u8; 16] {
        fn as_private_key(&self) -> &[u8] {
            self
        }
    }

    impl AsPrivateKey for [u8; 32] {
        fn as_private_key(&self) -> &[u8] {
            self
        }
    }

    impl AsPrivateKey for [u8; 64] {
        fn as_private_key(&self) -> &[u8] {
            self
        }
    }

    impl AsPrivateKey for VariableLengthSeed<'_> {
        fn as_private_key(&self) -> &[u8] {
            self.seed
        }
    }
}

/// Extended private keys derived using BIP-0032.
///
/// Generic over types implementing [`PrivateKey`].
#[derive(Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ExtendedPrivateKey<K: PrivateKey + Clone> {
    /// The internal private key data.
    #[serde(with = "serde_with")]
    private_key: K,
    depth: u8,
    chain_code: ChainCode,
}

mod serde_with {
    use super::*;

    pub(crate) fn serialize<S, K>(value: &K, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
        K: PrivateKey + Clone,
    {
        serializer.serialize_bytes(&value.to_bytes())
    }

    pub(crate) fn deserialize<'de, D, K>(deserializer: D) -> Result<K, D::Error>
    where
        D: serde::Deserializer<'de>,
        K: PrivateKey + Clone,
    {
        let variable_len_bytes = <&[u8]>::deserialize(deserializer)?;
        let bytes: [u8; 32] = variable_len_bytes.try_into().expect(bug!(
            "unable to parse serialized private key; no support for static len"
        ));
        Ok(K::from_bytes(&bytes).expect(bug!("could not deserialize key with invalid scalar")))
    }
}

impl<K: PrivateKey + Clone> std::fmt::Debug for ExtendedPrivateKey<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ExtendedPrivateKey")
            .field("private_key", &"obscured")
            .field("depth", &self.depth)
            .field("chain_code", &self.chain_code)
            .finish()
    }
}

impl<K> ExtendedPrivateKey<K>
where
    K: PrivateKey + Clone,
{
    /// Generate a new [`ExtendedPrivateKey`] from a seed, ideally from a 12-word or 24-word
    /// mnemonic, but may take 16-byte seeds.
    ///
    /// # Errors
    /// The function may return an error if the derived master key could not be parsed as a key for
    /// the given algorithm (such as exceeding values on the secp256k1 curve).
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// let seed: &[u8; 64] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::new(*seed);
    /// ```
    pub fn new(seed: impl as_private_key::AsPrivateKey) -> Result<Self> {
        Self::new_internal(seed.as_private_key())
    }

    fn new_internal(seed: &[u8]) -> Result<Self> {
        let hash = HmacSha512::new_from_slice(&K::key().bytes().collect::<Vec<_>>())
            .expect(bug!("HmacSha512 InvalidLength should be infallible"))
            .chain_update(seed)
            .finalize()
            .into_bytes();
        let (private_key, chain_code) = hash.split_at(KEY_SIZE / 8);

        // Verify the master key is nonzero, hopefully avoiding side-channel attacks.
        let mut has_any_nonzero = false;
        // deoptimize arithmetic smartness
        for byte in private_key.iter().map(std::hint::black_box) {
            if *byte != 0 {
                // deoptimize break
                has_any_nonzero = std::hint::black_box(true);
            }
        }

        assert!(has_any_nonzero, bug!("hmac function returned all-zero master key"));

        Self::from_parts(
            private_key
                .try_into()
                .expect(bug!("KEY_SIZE / 8 did not give a 32 byte slice")),
            0,
            // Checked: chain_code is always the same length, hash is static size
            chain_code
                .try_into()
                .expect(bug!("Invalid chain code length")),
        )
    }

    /// Create an [`ExtendedPrivateKey`] from a given `seed`, `depth`, and `chain_code`.
    ///
    /// # Errors
    /// The function may error if a private key can't be created from the seed.
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// let key: &[u8; 32] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let chain_code: &[u8; 32] = //
    /// #   b"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::from_parts(key, 4, *chain_code);
    /// ```
    pub fn from_parts(key: &[u8; 32], depth: u8, chain_code: [u8; 32]) -> Result<Self> {
        match K::from_bytes(key) {
            Ok(key) => {
                Ok(Self {
                    private_key: key,
                    depth,
                    chain_code,
                })
            }
            Err(_) => Err(Error::InvalidKey),
        }
    }

    /// Returns a reference to the [`PrivateKey`].
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   private_key::PrivateKey as _,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let key: &[u8; 32] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let chain_code: &[u8; 32] = //
    /// #   b"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::from_parts(key, 4, *chain_code)?;
    /// assert_eq!(xprv.private_key(), &PrivateKey::from_bytes(key)?);
    /// # Ok(())
    /// # }
    /// ```
    pub fn private_key(&self) -> &K {
        &self.private_key
    }

    /// Create an [`ExtendedPublicKey`] for the current [`PrivateKey`].
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   private_key::PrivateKey as _,
    /// #   public_key::PublicKey as _,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let seed: &[u8; 64] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// # let known_key: [u8; 33] = [
    /// # 0, 242, 26, 9, 159, 68, 199, 0, 206, 71, 248,
    /// # 102, 201, 210, 159, 219, 222, 42, 201, 44, 196, 27,
    /// # 90, 221, 80, 85, 135, 79, 39, 253, 223, 35, 251
    /// # ];
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::new(*seed)?;
    /// let xpub = xprv.extended_public_key();
    /// assert_eq!(known_key, xpub.public_key().to_bytes());
    /// # Ok(())
    /// # }
    /// ```
    pub fn extended_public_key(&self) -> ExtendedPublicKey<K::PublicKey> {
        ExtendedPublicKey::from_parts(self.public_key(), self.depth, self.chain_code)
    }

    /// Return a public key for the current [`PrivateKey`].
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   private_key::PrivateKey as _,
    /// #   public_key::PublicKey as _,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let seed: &[u8; 64] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::new(*seed)?;
    /// let pubkey = xprv.public_key();
    /// # Ok(())
    /// # }
    /// ```
    pub fn public_key(&self) -> K::PublicKey {
        self.private_key.public_key()
    }

    /// Returns the current depth.
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let key: &[u8; 32] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let chain_code: &[u8; 32] = //
    /// #   b"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::from_parts(key, 4, *chain_code)?;
    /// assert_eq!(xprv.depth(), 4);
    /// # Ok(())
    /// # }
    /// ```
    pub fn depth(&self) -> u8 {
        self.depth
    }

    /// Returns a copy of the current chain code.
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let key: &[u8; 32] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let chain_code: &[u8; 32] = //
    /// #   b"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";
    /// let xprv = ExtendedPrivateKey::<PrivateKey>::from_parts(key, 4, *chain_code)?;
    /// assert_eq!(chain_code, &xprv.chain_code());
    /// # Ok(())
    /// # }
    /// ```
    pub fn chain_code(&self) -> [u8; 32] {
        self.chain_code
    }

    /// Derive a child using the given [`DerivationPath`].
    ///
    /// # Errors
    /// An error may be returned under the same circumstances as
    /// [`ExtendedPrivateKey::derive_child`].
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let seed: &[u8; 64] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let root_xprv = ExtendedPrivateKey::<PrivateKey>::new(*seed)?;
    /// let path = DerivationPath::default()
    ///     .chain_push(DerivationIndex::new(44, true)?)
    ///     .chain_push(DerivationIndex::new(0, true)?)
    ///     .chain_push(DerivationIndex::new(0, true)?)
    ///     .chain_push(DerivationIndex::new(0, false)?);
    /// let derived_xprv = root_xprv.derive_path(&path)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn derive_path(&self, path: &DerivationPath) -> Result<Self> {
        if path.path.is_empty() {
            Ok(self.clone())
        } else {
            path.iter()
                .try_fold(self.clone(), |key, index| key.derive_child(index))
        }
    }

    /// Derive a child with a given [`DerivationIndex`].
    ///
    /// # Panics
    ///
    /// The method performs unchecked `try_into()` operations on constant-sized slice.
    ///
    /// # Errors
    ///
    /// An error may be returned if:
    ///
    /// * The depth exceeds the maximum depth [`u8::MAX`].
    /// * A `HmacSha512` can't be constructed - this should be impossible.
    /// * Deriving a child key fails. Check the documentation for your [`PrivateKey`].
    ///
    /// # Examples
    /// ```rust
    /// # use keyfork_derive_util::{
    /// #   *,
    /// #   public_key::TestPublicKey as PublicKey,
    /// #   private_key::TestPrivateKey as PrivateKey,
    /// # };
    /// # fn check_empty(p: &ExtendedPrivateKey<PrivateKey>) -> Result<(), std::io::Error> {
    /// # Ok(())
    /// # }
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// let seed: &[u8; 64] = //
    /// #   b"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    /// let root_xprv = ExtendedPrivateKey::<PrivateKey>::new(*seed)?;
    /// let bip44_wallet = DerivationPath::default()
    ///     .chain_push(DerivationIndex::new(44, true)?)
    ///     .chain_push(DerivationIndex::new(0, true)?)
    ///     .chain_push(DerivationIndex::new(0, true)?)
    ///     .chain_push(DerivationIndex::new(0, false)?);
    /// let change_xprv = root_xprv.derive_path(&bip44_wallet)?;
    /// for account in (0..20).map(|i| DerivationIndex::new(i, false).unwrap()) {
    ///     let account_xprv = change_xprv.derive_child(&account)?;
    ///     check_empty(&account_xprv)?;
    /// }
    /// # Ok(())
    /// # }
    pub fn derive_child(&self, index: &DerivationIndex) -> Result<Self> {
        let depth = self.depth.checked_add(1).ok_or(Error::Depth)?;

        let mut hmac = HmacSha512::new_from_slice(&self.chain_code)
            .expect(bug!("HmacSha512 InvalidLength should be infallible"));
        if index.is_hardened() {
            hmac.update(&[0]);
            hmac.update(&self.private_key.to_bytes());
        } else if !K::requires_hardened_derivation() {
            hmac.update(&self.private_key.public_key().to_bytes());
        } else {
            return Err(Error::HardenedDerivationRequired);
        }
        hmac.update(&index.to_bytes());
        let result = hmac.finalize().into_bytes();
        let (private_key, chain_code) = result.split_at(KEY_SIZE / 8);

        let private_key = self
            .private_key
            .derive_child(
                &private_key
                    .try_into()
                    .expect(bug!("Invalid length for private key")),
            )
            .map_err(|_| Error::Derivation)?;

        Ok(Self {
            private_key,
            depth,
            chain_code: chain_code
                .try_into()
                .expect(bug!("Invalid length for chain code")),
        })
    }
}
