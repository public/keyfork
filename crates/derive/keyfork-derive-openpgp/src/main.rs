//! Query the Keyfork Servre to derive an OpenPGP Secret Key.

use std::{env, process::ExitCode, str::FromStr};

use keyfork_derive_util::DerivationPath;
use keyfork_derive_path_data::paths;
use keyforkd_client::Client;

use ed25519_dalek::SigningKey;
use sequoia_openpgp::{
    armor::{Kind, Writer},
    packet::UserID,
    serialize::Marshal,
    types::KeyFlags,
};

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error("Bad character: {0}")]
    BadChar(char),
}

#[derive(Debug)]
struct KeyType {
    inner: KeyFlags,
}

impl Default for KeyType {
    fn default() -> Self {
        Self {
            inner: KeyFlags::empty(),
        }
    }
}

impl KeyType {
    fn certify(mut self) -> Self {
        self.inner = self.inner.set_certification();
        self
    }

    fn sign(mut self) -> Self {
        self.inner = self.inner.set_signing();
        self
    }

    fn encrypt(mut self) -> Self {
        self.inner = self.inner.set_transport_encryption();
        self.inner = self.inner.set_storage_encryption();
        self
    }

    fn authenticate(mut self) -> Self {
        self.inner = self.inner.set_authentication();
        self
    }

    fn inner(&self) -> &KeyFlags {
        &self.inner
    }
}

impl FromStr for KeyType {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.chars().try_fold(Self::default(), |s, ch| match ch {
            'C' | 'c' => Ok(s.certify()),
            'S' | 's' => Ok(s.sign()),
            'E' | 'e' => Ok(s.encrypt()),
            'A' | 'a' => Ok(s.authenticate()),
            ch => Err(Error::BadChar(ch)),
        })
    }
}

fn validate(
    path: &str,
    subkey_format: &str,
    default_userid: &str,
) -> Result<(DerivationPath, Vec<KeyType>, UserID), Box<dyn std::error::Error>> {
    let index = paths::OPENPGP.inner().first().unwrap();

    let path = DerivationPath::from_str(path)?;
    assert!(path.len() >= 2, "Expected path of at least m/{index}/account_id'");

    let given_index = path.iter().next().expect("checked .len() above");
    assert_eq!(
        index, given_index,
        "Expected derivation path starting with m/{index}, got: {given_index}",
    );

    let subkey_format = subkey_format
        .split(',')
        .map(KeyType::from_str)
        .collect::<Result<Vec<_>, Error>>()?;
    assert!(
        subkey_format[0].inner().for_certification(),
        "First key must be able to certify"
    );

    Ok((path, subkey_format, UserID::from(default_userid)))
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let mut args = env::args();
    let program_name = args.next().expect("program name");
    let args = args.collect::<Vec<_>>();
    let (path, subkey_format, default_userid) = match args.as_slice() {
        [path, subkey_format, default_userid] => validate(path, subkey_format, default_userid)?,
        _ => panic!("Usage: {program_name} path subkey_format default_userid"),
    };

    let derived_xprv = Client::discover_socket()?.request_xprv::<SigningKey>(&path)?;
    let subkeys = subkey_format
        .iter()
        .map(|kt| kt.inner().clone())
        .collect::<Vec<_>>();

    let cert = keyfork_derive_openpgp::derive(derived_xprv, subkeys.as_slice(), &default_userid)?;

    let mut w = Writer::new(std::io::stdout(), Kind::SecretKey)?;

    for packet in cert.as_tsk().into_packets() {
        packet.serialize(&mut w)?;
    }

    w.finalize()?;

    Ok(())
}

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("Error: {e}");
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}
