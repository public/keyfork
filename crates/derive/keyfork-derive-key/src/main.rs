//! Query the Keyfork Server to generate a hex-encoded key for a given algorithm.

use std::{env, process::ExitCode, str::FromStr};

use keyfork_derive_util::{
    request::{DerivationAlgorithm, DerivationError, DerivationRequest, DerivationResponse},
    DerivationPath, PathError,
};
use keyforkd_client::Client;

/// Any error that can occur while deriving a key.
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// The given algorithm could not be parsed.
    #[error("Could not parse the given algorithm {0:?}: {1}")]
    AlgoFormat(String, DerivationError),

    /// The given path could not be parsed.
    #[error("Could not parse the given path: {0}")]
    PathFormat(#[from] PathError),

    /// The request to derive data failed.
    #[error("Unable to perform key derivation request: {0}")]
    KeyforkdClient(#[from] keyforkd_client::Error),
}

#[allow(missing_docs)]
pub type Result<T, E = Error> = std::result::Result<T, E>;

fn validate(algo: &str, path: &str) -> Result<(DerivationAlgorithm, DerivationPath)> {
    let algo =
        DerivationAlgorithm::from_str(algo).map_err(|e| Error::AlgoFormat(algo.to_string(), e))?;
    let path = DerivationPath::from_str(path)?;
    Ok((algo, path))
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let mut args = env::args();
    let program_name = args.next().expect("program name");
    let args = args.collect::<Vec<_>>();
    let (algo, path) = match args.as_slice() {
        [algo, path] => validate(algo, path)?,
        _ => panic!("Usage: {program_name} algorithm path"),
    };

    let mut client = Client::discover_socket()?;
    let request = DerivationRequest::new(algo, &path);
    let response = client.request(&request.into())?;
    println!("{}", smex::encode(DerivationResponse::try_from(response)?.data));
    Ok(())
}

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("Error: {e}");
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}
