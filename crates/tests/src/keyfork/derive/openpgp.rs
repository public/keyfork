use sequoia_openpgp as openpgp;

use assert_cmd::Command;
use openpgp::{
    parse::{PacketParser, Parse},
    policy::StandardPolicy,
    types::KeyFlags,
    Cert,
};

const KEYFORK_BIN: &str = "keyfork";

#[test]
fn test() {
    let policy = StandardPolicy::new();

    let command_output = Command::cargo_bin(KEYFORK_BIN)
        .unwrap()
        .args([
            "derive",
            "openpgp",
            "Ryan Heywood (RyanSquared) <ryan@distrust.co>",
        ])
        .assert()
        .success();

    let packets = PacketParser::from_bytes(&command_output.get_output().stdout).unwrap();
    let cert = Cert::try_from(packets).unwrap();

    // assert the cert contains _any_ secret key data
    assert!(
        cert.is_tsk(),
        "exported key should contain secret key data, indicated by the key being a TSK"
    );

    // assert the correct keys were added in the correct order
    let mut key_formats = std::collections::HashSet::from([
        KeyFlags::empty().set_certification(),
        KeyFlags::empty().set_signing(),
        KeyFlags::empty()
            .set_transport_encryption()
            .set_storage_encryption(),
        KeyFlags::empty().set_authentication(),
    ]);
    let valid_cert = cert.with_policy(&policy, None).unwrap();
    for key in valid_cert.keys() {
        let flags = key.key_flags().unwrap();
        assert!(
            key_formats.remove(&flags),
            "could not find key flag set: {flags:?}"
        );
        key.alive().expect("is live after being generated");
        key.parts_into_secret().expect("has secret keys");
    }
    if !key_formats.is_empty() {
        panic!("remaining key formats: {key_formats:?}");
    }
}
